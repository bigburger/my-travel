<?php

namespace Form;

use Core\Core;
use Form\Entity\Form;
use Form\Entity\FormField;
use Logger\Logger;
use Logger\LogType;
use Router\Exception\ServerErrorException;

abstract class FormHandler
{

    public static function getForm(string $formName): Form
    {
        $form = null;
        $formArray = Core::getConfig('form', 'form', $formName);

        try {
            if (isset($formArray['id']) && is_string($formArray['id'])) {
                if (isset($formArray['fields']) && is_array($formArray['fields'])) {
                    $form = new Form($formArray['id']);

                    self::checkFormMethod($form, $formArray);
                    self::checkFormClass($form, $formArray);
                    self::checkFormAction($form, $formArray);
                    self::checkFormEnctype($form, $formArray);
                    self::addFields($form, $formArray);
                } else {
                    throw new ServerErrorException("Unknown or bad formatted fields for " . $formName . "form.");
                }
            } else {
                throw new ServerErrorException("Unknown or bad formatted id for " . $formName . "form.");
            }
        } catch (ServerErrorException $e) {
            $logger = new Logger();
            $logger->error($e->getMessage());
        }

        return $form;
    }

    /**
     * Check if method exist if yes set the new method.
     *
     * @param Form $form
     * @param array $formArray
     */
    private static function checkFormMethod(Form $form, array $formArray): void
    {
        if (isset($formArray['method']) && is_string($formArray['method'])) {
            $form->setMethod($formArray['method']);
        }
    }

    /**
     * Check if class exist if yes set the new class or classes.
     *
     * @param Form $form
     * @param array $formArray
     */
    private static function checkFormClass(Form $form, array $formArray): void
    {
        if (isset($formArray['class'])) {
            if (is_array($formArray['class'])) {
                $form->setClasses($formArray['class']);
            } elseif (is_string($formArray['class'])) {
                $form->addClass($formArray['class']);
            }
        }
    }

    /**
     * Check if action exist if yes set the new action.
     *
     * @param Form $form
     * @param array $formArray
     */
    private static function checkFormAction(Form $form, array $formArray): void
    {
        if (isset($formArray['action']) && is_string($formArray['action'])) {
            $form->setAction($formArray['action']);
        }
    }

    /**
     * Check if enctype exist if yes set the new enctype.
     *
     * @param Form $form
     * @param array $formArray
     */
    private static function checkFormEnctype(Form $form, array $formArray): void
    {
        if (isset($formArray['enctype']) && is_string($formArray['enctype'])) {
            $form->setEnctype($formArray['enctype']);
        }
    }

    /**
     * Add fields to the Form object.
     *
     * @param Form $form
     * @param array $formArray
     */
    private static function addFields(Form $form, array $formArray): void
    {
        foreach ($formArray['fields'] as $fieldArray) {
            $field = self::getField($fieldArray);
            $form->addField($field);
        }
    }

    /**
     * Get FormField object from form array.
     *
     * @param array $fieldArray
     * @return FormField
     */
    private static function getField(array $fieldArray): FormField
    {
        $field = null;

        try {
            if (isset($fieldArray['id']) && is_string($fieldArray['id'])) {
                if (isset($fieldArray['name']) && is_string($fieldArray['name'])) {
                    if (isset($fieldArray['type']) && is_string($fieldArray['type'])) {
                        $formFieldType = "Form\\Entity\\FormFieldType\\" .
                            ucfirst(mb_strtolower($fieldArray['type'])) . "FormField";
                        /** @var FormField $field */
                        $field = new $formFieldType($fieldArray['id'], $fieldArray['name']);
                        $field->formatter($fieldArray);
                    } else {
                        throw new ServerErrorException(
                            "Undefined field type on " . $fieldArray['id'] . "field."
                        );
                    }
                } else {
                    throw new ServerErrorException(
                        "Undefined field name on " . $fieldArray['id'] . "field."
                    );
                }
            } else {
                throw new ServerErrorException(
                    "Undefined field id."
                );
            }
        } catch (ServerErrorException $e) {
            $logger = new Logger();
            $logger->error($e->getMessage());
        }

        return $field;
    }

    /**
     * Upload a file on assets folder.
     *
     * @param array $file
     * @param string $fileUploadedName
     * @param string $uploadPath
     * @return bool
     */
    public static function uploadFile(array $file, string $fileUploadedName, string $uploadPath = ""): bool
    {
        if ($uploadPath != "" && substr($uploadPath, -1) != "/") {
            $uploadPath .= '/';
        }

        $fileUploadPath = $GLOBALS['BASE_DIR'] . 'public/assets/' . $uploadPath;

        if (!is_dir($fileUploadPath)) {
            mkdir($fileUploadPath, 0777, true);
        }

        if (move_uploaded_file($file["tmp_name"], $fileUploadPath . $fileUploadedName)) {
            return true;
        }

        return false;
    }
}
