<?php

namespace Form\Entity;

class Form
{
    private $id;
    private $classes;
    private $method;
    private $action;
    private $fields;
    private $enctype;

    /**
     * Form constructor.
     * @param $id
     * @param $method
     */
    public function __construct($id, $method = "POST")
    {
        $this->id = $id;
        $this->method = $method;
        $this->classes = [];
        $this->action = "";
        $this->fields = [];
        $this->enctype = "";
    }

    /**
     * Id getter.
     *
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Id setter.
     *
     * @param string $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * Classes getter.
     *
     * @return array
     */
    public function getClasses(): array
    {
        return $this->classes;
    }

    /**
     * Classes setter.
     *
     * @param array $classes
     */
    public function setClasses($classes): void
    {
        $this->classes = $classes;
    }

    /**
     * Add class to classes.
     *
     * @param string $class
     */
    public function addClass($class): void
    {
        $this->classes[] = $class;
        $this->classes = array_unique($this->classes);
    }

    /**
     * Method getter.
     *
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * Method setter.
     *
     * @param string $method
     */
    public function setMethod($method): void
    {
        $this->method = $method;
    }

    /**
     * Action getter.
     *
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     *  Action setter.
     *
     * @param string $action
     */
    public function setAction($action): void
    {
        $this->action = $action;
    }

    /**
     * Fields getter.
     *
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * Add field to fields.
     *
     * @param FormField $field
     */
    public function addField(FormField $field): void
    {
        $this->fields[$field->getName()] = $field;
    }

    /**
     * Enctype getter.
     *
     * @return string
     */
    public function getEnctype(): string
    {
        return $this->enctype;
    }

    /**
     * Enctype setter.
     *
     * @param string $enctype
     */
    public function setEnctype(string $enctype): void
    {
        $this->enctype = $enctype;
    }

    /**
     * Render array of the form field.
     *
     * @return array
     */
    public function render(): array
    {
        $form = [
            'id' => $this->id,
            'method' => $this->method,
            'action' => $this->action,
            'enctype' => $this->enctype,
            'class' => $this->classesRender(),
            'fields' => [],
        ];

        foreach ($this->fields as $field) {
            $form['fields'][$field->getName()] = $field->render();
        }

        return $form;
    }

    /**
     * Render of the classes.
     *
     * @return string
     */
    private function classesRender(): string
    {
        $classes = "";

        foreach ($this->classes as $class) {
            $classes .= $class . ' ';
        }

        return $classes;
    }
}
