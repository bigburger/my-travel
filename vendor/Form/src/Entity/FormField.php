<?php

namespace Form\Entity;

use Core\Core;
use Logger\Logger;
use Logger\LogType;
use Router\Exception\ServerErrorException;

abstract class FormField
{
    protected $id;
    protected $name;
    protected $classes;
    protected $value;
    protected $type;
    protected $required;
    protected $rules;
    protected $othersAttr;

    /**
     * FormField constructor.
     * @param string $id
     * @param string $name
     */
    public function __construct(string $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
        $this->type = "";
        $this->classes = [];
        $this->value = "";
        $this->required = false;
        $this->rules = [];
        $this->othersAttr = [];
    }

    /**
     * Id getter.
     *
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Id setter.
     *
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * Classes getter.
     *
     * @return array
     */
    public function getClasses(): array
    {
        return $this->classes;
    }

    /**
     * Classes setter.
     *
     * @param array $classes
     */
    public function setClasses(array $classes): void
    {
        $this->classes = $classes;
    }

    /**
     * Add class to classes.
     *
     * @param string $class
     */
    public function addClass(string $class): void
    {
        $this->classes[] = $class;
        $this->classes = array_unique($this->classes);
    }

    /**
     * Name getter.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Name setter.
     *
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * Value getter.
     *
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * Value setter.
     *
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->value = $value;
    }

    /**
     * Type getter.
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Required getter.
     *
     * @return bool
     */
    public function isRequired(): bool
    {
        return $this->required;
    }

    /**
     * Required setter.
     *
     * @param bool $required
     */
    public function setRequired(bool $required): void
    {
        $this->required = $required;
    }

    /**
     * Rules getter.
     *
     * @return array
     */
    public function getRules(): array
    {
        return $this->rules;
    }

    /**
     * Add rule to rules.
     *
     * @param string $ruleName
     * @param array $rule
     */
    public function addRule(string $ruleName, array $rule): void
    {
        $this->rules[$ruleName] = $rule;
        $this->rules = array_unique($this->rules);
    }

    /**
     * OthersAttr getter.
     *
     * @return array
     */
    public function getOthersAttr(): array
    {
        return $this->othersAttr;
    }

    /**
     * OthersAttr setter.
     *
     * @param array $othersAttr
     */
    public function setOthersAttr(array $othersAttr): void
    {
        $this->othersAttr = $othersAttr;
    }

    /**
     * Add attr to othersAttr.
     *
     * @param array $otherAttr
     */
    public function addOtherAttr(array $otherAttr): void
    {
        $this->othersAttr[] = $otherAttr;
        $this->othersAttr = array_unique($this->othersAttr);
    }

    /**
     * Format field with field array.
     *
     * @param array $fieldArray
     */
    public function formatter(array $fieldArray): void
    {
        $this->checkClasses($fieldArray);
        $this->checkValue($fieldArray);
        $this->checkRequired($fieldArray);
        $this->checkRules($fieldArray);
        $this->checkOthersAttr($fieldArray);
    }

    /**
     * Check if class exist if yes set the new class or classes.
     *
     * @param array $fieldArray
     */
    private function checkClasses(array $fieldArray): void
    {
        try {
            if (isset($fieldArray['class'])) {
                if (is_array($fieldArray['class'])) {
                    $this->setClasses($fieldArray['class']);
                } elseif (is_string($fieldArray['class'])) {
                    $this->addClass($fieldArray['class']);
                } else {
                    throw new ServerErrorException("Bad type on class attribute ! Must be an string or array.");
                }
            }
        } catch (ServerErrorException $e) {
            $logger = new Logger();
            $logger->error($e->getMessage());
        }
    }

    /**
     * Check if value exist if yes set the value.
     *
     * @param array $fieldArray
     */
    private function checkValue(array $fieldArray): void
    {
        if (isset($fieldArray['value'])) {
            $this->setValue($fieldArray['value']);
        }
    }

    /**
     * Check if required exist if yes set the required.
     *
     * @param array $fieldArray
     */
    private function checkRequired(array $fieldArray): void
    {
        if (isset($fieldArray['required']) && is_bool($fieldArray['required'])) {
            $this->setRequired($fieldArray['required']);
        }
    }

    /**
     * Check if rules exist if yes set the rules.
     *
     * @param array $fieldArray
     */
    private function checkRules(array $fieldArray): void
    {
        if (isset($fieldArray['rules']) && is_array($fieldArray['rules'])) {
            foreach ($fieldArray['rules'] as $ruleName => $ruleArray) {
                $rule = [];

                if (isset($ruleArray['errorMessage']) && is_string($ruleArray['errorMessage'])) {
                    $rule['errorMessage'] = $ruleArray['errorMessage'];
                }

                if (isset($ruleArray['value'])) {
                    $rule['value'] = $ruleArray['value'];
                }

                if (isset($ruleArray['operator']) && is_string($ruleArray['operator'])) {
                    $rule['operator'] = $ruleArray['operator'];
                }

                if (isset($rule['errorMessage']) || isset($rule['value']) || isset($rule['operator'])) {
                    $this->addRule($ruleName, $rule);
                }
            }
        }
    }

    /**
     * Check if other attr exist if yes set the new attributes.
     *
     * @param array $fieldArray
     */
    private function checkOthersAttr(array $fieldArray): void
    {
        if (isset($fieldArray['other']) && is_array($fieldArray['other'])) {
            $this->setOthersAttr($fieldArray['other']);
        }
    }

    /**
     * Render array of the form field.
     *
     * @return array
     */
    public function render(): array
    {
        return [
            'name' => $this->name,
            'id' => $this->id,
            'type' => $this->type,
            'class' => $this->classesRender(),
            'value' => $this->value,
            'required' => $this->required,
            'other' => $this->othersAttr,
            'rule' => $this->rules,
        ];
    }

    /**
     * Render of the classes.
     *
     * @return string
     */
    private function classesRender(): string
    {
        $classes = "";

        foreach ($this->classes as $class) {
            $classes .= $class . ' ';
        }

        return $classes;
    }

    /**
     * Check if the field it required & data exist.
     *
     * @param array $data
     * @return array
     */
    public function requiredRule(array $data): array
    {
        $state = [
            'status' => true,
            'errorMessage' => '',
        ];

        if (!array_key_exists($this->name, $data)) {
            $state['status'] = false;
            $state['errorMessage'] = $this->rules['required'] ?? 'The ' . $this->name . ' field is required';
        }

        return $state;
    }
}
