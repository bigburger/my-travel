<?php

namespace Form\Entity\FormFieldType;

use Form\Entity\FormField;

class DateFormField extends FormField
{
    public function __construct(string $id, string $name)
    {
        parent::__construct($id, $name);
        $this->type = 'date';
    }

    public function formatter(array $fieldArray): void
    {
        parent::formatter($fieldArray);
    }

    public function render(): array
    {
        return parent::render();
    }

    /**
     * Check min length of the data.
     *
     * @param array $data
     * @return array
     */
    public function minLengthRule(array $data): array
    {
        $state = [
            'status' => false,
            'errorMessage' => 'MinLength rule must have a value attribute on ' . $this->name . ' field.',
        ];

        if (isset($this->getRules()['minLength']['value']) && is_int($this->getRules()['minLength']['value'])) {
            if ($this->getRules()['minLength']['value'] < mb_strlen($data[$this->getName()])) {
                $state = [
                    'status' => true,
                    'errorMessage' => '',
                ];
            } else {
                $state = [
                    'status' => false,
                    'errorMessage' => 'Too few characters on ' . $this->name . ' field.',
                ];
            }
        } else {
            $state = [
                'status' => false,
                'errorMessage' => 'Bad value attribute on minLength rule ' .
                    $this->name . ' field. Must be an integer.',
            ];
        }

        return $state;
    }

    /**
     * Check max length of the data.
     *
     * @param array $data
     * @return array
     */
    public function maxLengthRule(array $data): array
    {
        $state = [
            'status' => false,
            'errorMessage' => 'MaxLength rule must have a value attribute on ' . $this->name . ' field.',
        ];

        if (isset($this->getRules()['maxLength']['value']) && is_int($this->getRules()['maxLength']['value'])) {
            if ($this->getRules()['maxLength']['value'] > mb_strlen($data[$this->getName()])) {
                $state = [
                    'status' => true,
                    'errorMessage' => '',
                ];
            } else {
                $state = [
                    'status' => false,
                    'errorMessage' => 'Too much characters on ' . $this->name . ' field.',
                ];
            }
        } else {
            $state = [
                'status' => false,
                'errorMessage' => 'Bad value attribute on maxLength rule ' .
                    $this->name . ' field. Must be an integer.',
            ];
        }

        return $state;
    }
}
