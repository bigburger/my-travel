<?php

namespace Form\Entity\FormFieldType;

use Form\Entity\FormField;

class CheckboxFormField extends FormField
{
    public function __construct(string $id, string $name)
    {
        parent::__construct($id, $name);
        $this->type = 'checkbox';
    }

    public function formatter(array $fieldArray): void
    {
        parent::formatter($fieldArray);
    }

    public function render(): array
    {
        return parent::render();
    }
}
