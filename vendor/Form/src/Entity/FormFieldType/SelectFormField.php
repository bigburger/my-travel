<?php

namespace Form\Entity\FormFieldType;

use Form\Entity\FormField;

class SelectFormField extends FormField
{
    private $options;

    public function __construct(string $id, string $name)
    {
        parent::__construct($id, $name);
        $this->type = 'select';
        $this->options = [];
    }

    public function formatter(array $fieldArray): void
    {
        parent::formatter($fieldArray);
        $this->checkOptions($fieldArray);
    }

    public function render(): array
    {
        $field = parent::render();
        $field['options'] = $this->optionsRender();

        return $field;
    }

    /**
     * Render options.
     *
     * @return array
     */
    private function optionsRender(): array
    {
        $options = [];
        foreach ($this->options as $option) {
            $currentOption = [
                'value' => $option['value'],
                'desc' => $option['desc'],
            ];

            $currentOptionClasses = "";

            if (isset($option['class']) && is_array($option['class'])) {
                foreach ($option['class'] as $class) {
                    $currentOptionClasses .= $class . ' ';
                }
            }

            $currentOption['class'] = $currentOptionClasses;

            $options[] = $currentOption;
        }

        return $options;
    }

    /**
     * Options getter.
     *
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * Options setter.
     *
     * @param $options
     */
    public function setOptions(array $options): void
    {
        $this->options = $options;
    }

    /**
     * Add option to options.
     *
     * @param array $option
     */
    public function addOption(array $option): void
    {
        $this->options[] = $option;
    }

    /**
     * Check if class exist if yes set the new class or classes.
     *
     * @param array $fieldArray
     */
    private function checkOptions(array $fieldArray): void
    {
        if (isset($fieldArray['options']) && is_array($fieldArray['options'])) {
            foreach ($fieldArray['options'] as $optionArray) {
                if (isset($optionArray['value'])) {
                    if (isset($optionArray['desc'])) {
                        $option = [
                            'value' => $optionArray['value'],
                            'desc' => $optionArray['desc'],
                            'class' => []
                        ];

                        if (isset($optionArray['class'])) {
                            if (is_array($optionArray['class'])) {
                                $option['class'] = $optionArray['class'];
                            } elseif (is_string($optionArray['class'])) {
                                $option[] = $optionArray['class'];
                            }
                        }

                        $this->addOption($option);
                    }
                }
            }
        }
    }
}
