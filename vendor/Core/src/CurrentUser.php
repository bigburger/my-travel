<?php

namespace Core;

use ORM\ORMHandler;
use App\Entity\Users;

class CurrentUser
{
    private static $instance = null;
    private static $currentUser;

    private function __construct()
    {
        $this->generateCurrentUser();
    }

    /**
     * Get CurrentUser instance.
     *
     * @return \Core\CurrentUser|null
     */
    public static function getInstance(): ?CurrentUser
    {
        if (!self::$instance) {
            self::$instance = new CurrentUser();
        }
        return self::$instance;
    }

    /**
     * Get currentUser attribute.
     *
     * @return \App\Entity\Users|boolean
     */
    public function getCurrentUser()
    {
        return self::$currentUser ?? new Users();
    }

    /**
     * Destroy the currentUser attribute
     */
    public static function destroyCurrentUser()
    {
        self::$currentUser = null;
    }

    /**
     * generate the currentUser attribute using the USER_ID in session
     */
    public static function generateCurrentUser()
    {
        $currentUser = ORMHandler::getOneEntityBy(new Users(), [
            ["id", "=", $_SESSION['USER_ID']]
        ]);

        if ($currentUser == false) {
            $currentUser = new Users();
        }

        self::$currentUser = $currentUser;
        return $currentUser;
    }
}
