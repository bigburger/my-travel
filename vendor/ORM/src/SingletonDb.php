<?php

namespace ORM;

use Core\Core;
use Logger\Logger;
use Router\Exception\ServerErrorException;

class SingletonDb
{
    private static $instance = null;
    private $connection;

    private function __construct()
    {
        try {
            $this->connection = new \PDO(
                Core::getConfig('database.driver') .
                ':host=' . Core::getConfig('database.host') .
                ';port=' . Core::getConfig('database.port') .
                ';dbname=' . Core::getConfig('database.name'),
                Core::getConfig('database.user'),
                Core::getConfig('database.password')
            );
            $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $this->connection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
        } catch (ServerErrorException $e) {
            $logger = new Logger();
            $logger->error($e->getMessage());
        }
    }

    /**
     * Get database instance.
     *
     * @return \ORM\SingletonDb|null
     */
    public static function getInstance(): ?SingletonDb
    {
        if (!self::$instance) {
            self::$instance = new SingletonDb();
        }
        return self::$instance;
    }

    /**
     * Get PDO connection.
     *
     * @return \PDO
     */
    public function getConnection(): \PDO
    {
        return $this->connection;
    }
}
