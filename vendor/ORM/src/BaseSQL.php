<?php

namespace ORM;

use Core\Core;
use Logger\Logger;
use Router\Exception\ServerErrorException;

class BaseSQL
{
    private $pdo;
    private $table;

    public function __construct()
    {
        $this->pdo = SingletonDb::getInstance()->getConnection();
        $this->table = substr(strrchr(get_called_class(), "\\"), 1);
    }

    /**
     * Get columns table.
     *
     * @return array
     */
    public function getColumns(): array
    {
        $objectVars = get_object_vars($this);
        $classVars = get_class_vars(get_class());
        $columns = array_diff_key($objectVars, $classVars);
        unset($columns['ignoredFields']);
        return $columns;
    }

    /**
     * Save the entity in database.
     *
     * @return int|bool
     */
    public function save()
    {
        $columns = $this->getColumns();

        if (is_null($columns["id"])) {
            // INSERT query.

            unset($columns['id']);
            // unset and ignores all the unnecesary fields to insert
            if (gettype($this->ignoredFields) == 'array') {
                foreach ($this->ignoredFields as $key => $value) {
                    unset($columns[$value]);
                }
            }

            $sql = "INSERT INTO " . $this->table .
            " (" . implode(",", array_keys($columns)) .
            ") VALUES (:" . implode(",:", array_keys($columns)) . ")";
            $query = $this->pdo->prepare($sql);
            $query->execute($columns);
            if ($this->pdo->lastInsertId()) {
                return $this->pdo->lastInsertId();
            }
            return false;
        } else {
            // UPDATE query.

            // unset and ignores all the unnecesary fields to update
            foreach ($this->ignoredFields as $key => $value) {
                unset($columns[$value]);
            }
            unset($columns['ignoredFields']);

            foreach ($columns as $key => $value) {
                $sqlSet[] = $key . "=:" . $key;
            }
            $sql = "UPDATE " . $this->table . " SET " . implode(",", $sqlSet) . " WHERE id=:id";

            $query = $this->pdo->prepare($sql);
            return $query->execute($columns);
        }
    }

    /**
     * Get one entity by where clause.
     *
     * @param array $whereClause
     *
     * @return \ORM\Entity\BaseEntity or false
     */
    public function getOneBy(array $whereClause)
    {
        foreach ($whereClause as $key => $value) {
            $sqlWhere[] = ($value[1] == ' like ' ? 'LOWER('. $value[0] .')' : $value[0]) . $value[1] . ":" . $value[0];
            $whereClauseData[$value[0]] = $value[2];
        }

        $sql = "SELECT * FROM " . $this->table .
        " WHERE " . implode(" AND ", $sqlWhere) . " LIMIT 1";

        try {
            $query = $this->pdo->prepare($sql);
            $query->setFetchMode(\PDO::FETCH_CLASS, get_class($this));

            if ($query->execute($whereClauseData)) {
                return $query->fetch();
            } else {
                throw new ServerErrorException("Error while getting entity with : " . $sql);
            }
        } catch (ServerErrorException $e) {
            $logger = new Logger();
            $logger->error($e->getMessage());
        }
    }

    /**
     * Get all entities by where clause.
     *
     * @param array $whereClause
     * @param int|null $limit
     * @param string|null $orderBy
     *
     * @return \ORM\Entity\BaseEntityCollection|boolean
     */
    public function getAllBy(array $whereClause, int $limit = null, $orderBy = null)
    {
        foreach ($whereClause as $key => $value) {
            $sqlWhere[] = ($value[1] == ' like ' ? 'LOWER('. $value[0] .')' : $value[0]) . $value[1] . ":" . $value[0];
            $whereClauseData[$value[0]] = $value[2];
        }

        $sql = "SELECT * FROM " . $this->table .
        " WHERE " . implode(" AND ", $sqlWhere) . ($limit !== null ? " limit " . $limit : '') .
            ($orderBy !== null ? " ORDER BY " . $orderBy : '');

        try {
            $query = $this->pdo->prepare($sql);

            $query->setFetchMode(\PDO::FETCH_CLASS, get_class($this));

            if ($query->execute($whereClauseData)) {
                $entities = $query->fetchAll();

                $className = get_class($this) . "Collection";
                return new $className($entities);
            } else {
                throw new ServerErrorException("Error while getting entity with : " . $sql);
            }
        } catch (ServerErrorException $e) {
            $logger = new Logger();
            $logger->error($e->getMessage());
        }
    }

    /**
     * Get all entities.
     *
     * @param int|null $limit
     * @param string|null $orderBy
     *
     * @return \ORM\Entity\BaseEntityCollection|boolean
     */
    public function getAll(int $limit = null, $orderBy = null)
    {
        $sql = "SELECT * FROM " . $this->table . " " . ($limit !== null ? " limit " . $limit : '') .
            ($orderBy !== null ? " ORDER BY " . $orderBy : '');

        try {
            $query = $this->pdo->prepare($sql);

            $query->setFetchMode(\PDO::FETCH_CLASS, get_class($this));

            if ($query->execute()) {
                $entities = $query->fetchAll();

                $className = get_class($this) . "Collection";
                return new $className($entities);
            } else {
                throw new ServerErrorException("Error while getting entity with : " . $sql);
            }
        } catch (ServerErrorException $e) {
            $logger = new Logger();
            $logger->error($e->getMessage());
        }
    }

    /**
     * Delete one or many entities.
     *
     * @param array|null $whereClause
     *
     * @return boolean
     */
    public function delete(array $whereClause = null): bool
    {

        if ($whereClause) {
            foreach ($whereClause as $key => $value) {
                $sqlWhere[] = ($value[1] == ' like ' ? 'LOWER('. $value[0] .')' :
                        $value[0]) . $value[1] . ":" . $value[0];
                $whereClauseData[$value[0]] = $value[2];
            }
        }

        $sql = "DELETE FROM " . $this->getTable();
        if ($whereClause) {
            $sql = $sql . " WHERE " . implode(" AND ", $sqlWhere);
        }

        try {
            $query = $this->pdo->prepare($sql);
            return $query->execute($whereClauseData);
        } catch (ServerErrorException $e) {
            $logger = new Logger();
            $logger->error($e->getMessage());
        }
    }

    /**
     * Get table name.
     *
     * @return string
     */
    public function getTable(): string
    {
        return $this->table;
    }
}
