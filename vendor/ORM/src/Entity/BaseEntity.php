<?php

namespace ORM\Entity;

use ORM\BaseSQL;

class BaseEntity extends BaseSQL
{
    protected $id;
    protected $ignoredFields;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get entity id.
     *
     * @return null|integer
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set the entity id.
     *
     * @return null
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * Get entity ignored fields.
     *
     * @return array|integer
     */
    public function getIgnoredFields(): ?array
    {
        return $this->ignoredFields;
    }

    /**
     * Set the ignored fields array for the entity if it is saved
     *
     * @return null
     */
    public function setIgnoredFields(array $ignoredFields): void
    {
        $this->ignoredFields = $ignoredFields;
    }

    /**
     * Fill this entity with its attributes in database
     */
    public function fill(): void
    {
        if ($this->id) {
            $entity = $this->getOneBy([
                ["id", "=", $this->id]
            ]);
            $this->fetchInto($entity);
        }
    }

    /**
     * fetch object variable of the base entity passed in parameters into the current BaseEntity
     * for example, it is useful when loging a user in and retrieving all his variables
     *
     * @param BaseEntity $entity
     *
     * @return void
     */
    public function fetchInto(BaseEntity $entity): void
    {
        // iterate over all the variable of the entity and call the setter if the value is not null
        foreach (get_object_vars($entity) as $key => $value) {
            $setterName = 'set' . ucfirst($key);
            preg_match("/_([a-z]){1}/", $setterName, $matches);
            $setterName = preg_replace("/_([a-z]){1}/", ucfirst($matches[1]), $setterName);

            if ($value) {
                // if key correspond to a timestamp, we convert the string timestamp to a DateTime
                if (preg_match("/date/", $key)) {
                    $value = new \DateTime($value, new \DateTimeZone(date_default_timezone_get()));
                }
                $this->$setterName($value);
            }
        }
    }
}
