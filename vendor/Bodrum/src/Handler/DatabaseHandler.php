<?php

namespace Bodrum\Handler;

use Core\Core;

class DatabaseHandler
{
    private $dbFilePath;
    private $connection;

    public function __construct()
    {
        $this->dbFilePath = $GLOBALS['BASE_DIR'] . "docker/postgresql/db.sql";

        $this->connection = new \PDO(
            Core::getConfig('database.driver') .
            ':host=' . Core::getConfig('database.host') .
            ';port=' . Core::getConfig('database.port') .
            ';dbname=' . Core::getConfig('database.name'),
            Core::getConfig('database.user'),
            Core::getConfig('database.password')
        );
        $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $this->connection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
    }

    /**
     * Generate the database.
     *
     * @return bool
     */
    public function generate(): bool
    {
        try {
            $this->connection->exec(file_get_contents($this->dbFilePath));
            return true;
        } catch (Exception $e) {
            echo($e->getMessage());
        }

        return false;
    }

    /**
     * Destroy the database.
     *
     * @return bool
     */
    public function destroy(): bool
    {
        try {
            $this->connection->exec("DROP SCHEMA public CASCADE;CREATE SCHEMA public;");
            return true;
        } catch (Exception $e) {
            echo($e->getMessage());
        }

        return false;
    }

    /**
     * Rebuild the database.
     *
     * @return bool
     */
    public function rebuild(): bool
    {
        if ($this->destroy()) {
            if ($this->generate()) {
                return true;
            }
        }

        return false;
    }
}
