<?php

namespace Bodrum\Handler;

class LogsHandler
{
    private $dir;

    public function __construct()
    {
        $this->dir = $GLOBALS['BASE_DIR'] . "var/logs/";
    }

    /**
     * Show today log file.
     */
    public function show(): void
    {
        if (file_exists($this->dir)) {
            $files = scandir($this->dir, SCANDIR_SORT_DESCENDING);
            echo file_get_contents($this->dir . $files[0]);
        } else {
            echo "\033[91mThere are no logs.\e[37;0m\n";
        }
    }

    /**
     * Delete all logs files.
     */
    public function clear(): void
    {
        foreach (glob($this->dir . "*") as $file) {
            if (is_file($file)) {
                unlink($file);
                echo "\033[30m$file removed.\e[37;0m\n";
            }
        }

        echo "\033[92mLogs has been clear !\e[37;0m\n";
    }
}
