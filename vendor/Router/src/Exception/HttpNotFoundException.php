<?php

namespace Router\Exception;

use Router\Entity\ViewResponse;

class HttpNotFoundException extends \Exception
{
    /**
     * HttpNotFoundException destructor.
     */
    public function __destruct()
    {
        $message = $this->message . " On " . $this->file . " line " . $this->line . ".";
        new ViewResponse('errors.404', ['errorMessage' => $message], 404);
        exit();
    }
}
