<?php

namespace Router\Exception;

use Router\Entity\ViewResponse;

class ServerErrorException extends \Exception
{
    /**
     * ServerErrorException destructor.
     */
    public function __destruct()
    {
        $message = $this->message . " On " . $this->file . " line " . $this->line . ".";

        if ($this->file != $GLOBALS['BASE_DIR'] . "vendor/Core/src/Core.php") {
            new ViewResponse('errors.500', ['errorMessage' => $message], 500);
        } else {
            echo $message;
        }
        exit();
    }
}
