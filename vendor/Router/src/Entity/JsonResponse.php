<?php

namespace Router\Entity;

class JsonResponse extends Response
{
    private $json;

    /**
     * JsonResponse constructor.
     *
     * @param string $json
     * @param int $statusCode
     * @param array $headers
     */
    public function __construct(string $json, int $statusCode = null, array $headers = [])
    {
        parent::__construct($statusCode ?? 200, $headers);
        $this->json = $json;
    }

    /**
     *  Get JSON content.
     *
     * @return string
     */
    public function getJson(): string
    {
        return $this->json;
    }

    /**
     * JsonResponse destructor.
     */
    public function __destruct()
    {
        parent::__destruct();
        header('Content-Type: application/json');
        echo $this->json;
    }
}
