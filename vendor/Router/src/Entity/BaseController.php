<?php

namespace Router\Entity;

class BaseController
{
    protected $request;

    /**
     * BaseController constructor
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
}
