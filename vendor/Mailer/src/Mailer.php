<?php

namespace Mailer;

use Mailer\Entity\Mail;

abstract class Mailer
{
    private static function generateHeaders(Mail $mail): string
    {
        $headers = [];
        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=iso-8859-1';
        $headers[] = 'From: ' . $mail->getSender();
        $headers[] = 'Cc: ' . implode(',', $mail->getCc());
        $headers[] = 'Bcc: ' . implode(',', $mail->getBcc());

        return implode("\r\n", $headers);
    }

    public static function send(Mail $mail): bool
    {
        $headers = self::generateHeaders($mail);

        return mail(
            implode(',', $mail->getReceivers()),
            $mail->getSubject(),
            wordwrap($mail->getMessage(), 70, "\r\n"),
            $headers
        );
    }
}
