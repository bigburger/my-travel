<?php

namespace Mailer\Entity;

class Mail
{
    private $sender;
    private $subject;
    private $message;
    private $receivers;
    private $cc;
    private $bcc;

    /**
     * Mail constructor.
     * @param string $sender
     * @param string $subject
     * @param string $message
     */
    public function __construct(string $sender, string $subject, string $message)
    {
        $this->sender = $sender;
        $this->subject = $subject;
        $this->message = $message;
        $this->receivers = [];
        $this->cc = [];
        $this->bcc = [];
    }


    /**
     * @return string
     */
    public function getSender(): string
    {
        return $this->sender;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return array
     */
    public function getReceivers(): array
    {
        return $this->receivers;
    }

    /**
     * @return array
     */
    public function getCc(): array
    {
        return $this->cc;
    }

    /**
     * @return array
     */
    public function getBcc(): array
    {
        return $this->bcc;
    }

    public function addReceiver(string $receiver)
    {
        $this->receivers[] = $receiver;
        $this->receivers = array_unique($this->receivers);
    }

    public function addCc(string $cc)
    {
        $this->cc[] = $cc;
        $this->cc = array_unique($this->cc);
    }

    public function addBcc(string $bcc)
    {
        $this->bcc[] = $bcc;
        $this->bcc = array_unique($this->bcc);
    }
}
