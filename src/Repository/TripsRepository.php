<?php

namespace App\Repository;

use ORM\ORMHandler;
use App\Entity\Trips;

class TripsRepository
{

    /**
     * Create a trip
     *
     * @return int|bool
     */
    public function createTrip($tripData)
    {
        $trip = new Trips();
        $trip->setTitle($tripData['title']);
        $trip->setDescription($tripData['description']);
        $trip->setImagePath($tripData['imagePath']);
        $trip->setUserId($tripData['userId']);
        $trip->setStartingDate($tripData['startingDate']);
        if ($tripData['endingDate']) {
            $trip->setEndingDate($tripData['endingDate']);
        }
        return $trip->save();
    }

    /**
     * Get the latest
     *
     * @return array
     */
    public function lastHomeTrips(): array
    {
        $trips = ORMHandler::getAllEntities(new Trips(), 6);
        return $trips->getEntities();
    }

    /**
     * Get all trips
     *
     * @return array
     */
    public function getAllTrips($orderBy = null): array
    {
        $trips = ORMHandler::getAllEntities(new Trips(), null, $orderBy);
        return $trips->getEntities();
    }

    /**
     * Get all trips by
     *
     * @param array $where
     * @param string $orderBy
     * @return array
     */
    public function getAllTripsBy(array $where, $orderBy = null): array
    {
        $trips = ORMHandler::getAllEntitiesBy(new Trips(), $where, null, $orderBy);
        return $trips->getEntities();
    }

    /**
     * Get one trip by
     *
     * @param array $where
     * @return bool|\ORM\Entity\BaseEntity
     */
    public function getOneTripBy(array $where)
    {
        $trip = ORMHandler::getOneEntityBy(new Trips(), $where);
        return $trip;
    }

    /**
     * Get all trips where the search query parameter matches with a trip's title
     *
     * @param string $searchQuery
     * @return array
     */
    public function getAllTripsLike($searchQuery): array
    {
        $trips = ORMHandler::getAllEntitiesBy(new Trips(), [
            ["title", " like ", "%". strtolower(htmlspecialchars($searchQuery)) ."%"]
        ]);
        return $trips->getEntities();
    }

    /**
     * Delete a trip
     *
     * @param array $where
     * @return boolean
     */
    public function deleteTrip(array $where): bool
    {
        return ORMHandler::deleteEntities(new Trips(), $where);
    }

    /**
     * Get all trips using tags like the query parameter in first and then trips matching title with the query parameter
     *
     * @param string $searchQuery
     * @return array
     */
    public function getAllTripsUsingTagsLike($searchQuery): array
    {
        $trips = [];
        $tagsRepo = new TagsRepository();
        $tripsTagsRepo = new TripsTagsRepository();

        $tags = $tagsRepo->getAllTagsLike($searchQuery);

        foreach ($tags as $tag) {
            $tripsTags = $tripsTagsRepo->getAllTripsTagsBy([["tag_id", "=", $tag->getId()]]);

            foreach ($tripsTags as $tripTag) {
                $trip = $this->getOneTripBy([["id", "=", $tripTag->getTripId()]]);

                // Checking if the trip is not already in the array because
                // it may be if several tags have a same portion of text in their labels
                if (!in_array($trip, $trips)) {
                    $trips[] = $trip;
                }
            }
        }

        return array_reverse(array_unique(array_merge($trips, $this->getAllTripsLike($searchQuery))));
    }

    /**
     * Get all trips using a tag id
     *
     * @param string $tagId
     * @return array
     */
    public function getAllTripsUsingTagId($tagId): array
    {
        $trips = [];
        $tagsRepo = new TagsRepository();
        $tripsTagsRepo = new TripsTagsRepository();

        $tag = $tagsRepo->getOneTagBy([["id", "=", $tagId]]);

        if (!$tag) {
            return [];
        }

        $tripsTags = $tripsTagsRepo->getAllTripsTagsBy([["tag_id", "=", $tag->getId()]]);

        foreach ($tripsTags as $tripTag) {
            $trip = $this->getOneTripBy([["id", "=", $tripTag->getTripId()]]);
            $trips[] = $trip;
        }

        return $trips;
    }

    /**
     * Update a trip
     *
     * @param string $tripData
     */
    public function editTrip($tripData)
    {
        $trip = $this->getOneTripBy([['id', '=', $tripData['trip-id']]]);
        $trip->setTitle($tripData['trip-title']);
        $trip->setDescription($tripData['trip-description']);
        $trip->setImagePath($tripData['trip-image-path']);
        $trip->setEndingDate($tripData['trip-ending-date']);

        $trip->save();
    }
}
