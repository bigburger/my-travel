<?php

namespace App\Repository;

use ORM\ORMHandler;
use App\Entity\Tags;

class TagsRepository
{

    /**
     * Post one tag
     *
     */
    public function createTag($tagData)
    {
        $tag = new Tags();
        $tag->setLabel($tagData["label"]);
        $tag->save();

        return $this->getOneTagBy([
            ["label", "=", htmlspecialchars($tagData["label"])]
        ]);
    }

    /**
     * Get all tags
     *
     * @return array
     */
    public function getAllTags($orderBy = null): array
    {
        $tags = ORMHandler::getAllEntities(new Tags(), null, $orderBy);
        return $tags->getEntities();
    }

    /**
     * Get all tags by
     *
     * @return array
     */
    public function getAllTagsBy($where): array
    {
        $tags = ORMHandler::getAllEntitiesBy(new Tags(), $where);
        return $tags->getEntities();
    }

    /**
     * Get one tag by
     *
     * @return bool|\ORM\Entity\BaseEntity
     */
    public function getOneTagBy($where)
    {
        $tag = ORMHandler::getOneEntityBy(new Tags(), $where);
        return $tag;
    }

    /**
     * Get used tags of a trip
     *
     * @return array
     */
    public function getTagsUsedByTrip($tripId)
    {
        $usedTags = [];
        $tripsTagsRepo = new TripsTagsRepository();
        $tripsTags = $tripsTagsRepo->getAllTripsTagsBy([
            ["trip_id", "=", $tripId]
        ]);

        foreach ($tripsTags as $tripTag) {
            $usedTag = $this->getOneTagBy([["id", "=", $tripTag->getTagId()]]);
            $usedTags[] = $usedTag;
        }

        return $usedTags;
    }

    /**
     * Get all tags with a search query parameter
     *
     * @param string $searchQuery
     * @return array
     */
    public function getAllTagsLike($searchQuery): array
    {
        $tags = ORMHandler::getAllEntitiesBy(new Tags(), [
            ["label", " like ", "%". strtolower(htmlspecialchars($searchQuery)) ."%"]
        ]);
        return $tags->getEntities();
    }
}
