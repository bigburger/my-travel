{% import templates.front.front %}

{% block content %}
<main>
    <section id="cover">
        <div class="search-travel-bg-img bg-image-text vh-30">
            <img class="logo-small" src="/assets/images/logos/logo-secondary.svg" alt="Logo">
        </div>
    </section>
    <?php if ($successMessage) : ?>
        <div class="alert alert-success alert-bottom-right">
            <p><?= Core\Core::translate('message.success', $successMessage); ?></p>  
            <span class="close-alert">&times;</span>
        </div>
    <?php endif; ?>
    <div class="container <?= count($trips) == 0 ? 'min-height-container' : '' ?>">
        <div class="row">
            <div class="<?= count($trips) == 0 ? 'col-4 offset-4' : 'col-3 offset-2 col-xl-8 offset-xl-2' ?>">
                <section id="travel-info">
                    <div class="card">
                        <div class="card-body">
                            <?php if(count($trips) == 0): ?>
                                <div class="text-center">
                                    <p><?= Core\Core::translate('user.trips', 'anyTripYet') ?></p>
                                    <a href="{% url user.show_create_trip_form %}" class="btn btn-primary m-t-20"><?= Core\Core::translate('user.trips', 'postTrip') ?></a>
                                </div>
                            <?php else: ?>
                                <p><?= Core\Core::translate('user.trips', 'numberTrip') ?>  <?= count($trips) ?></p>
                                <p><?= Core\Core::translate('user.trips', 'numberStep') ?>  <?= $stepsCount ?></p>
                            <?php endif; ?>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-5 col-xl-8 offset-xl-2">
                <section id="publications">
                    <?php foreach(array_reverse($trips) as $key => $trip): ?>
                        <div class="card">
                            <div class="card-body d-flex flex-wrap">
                                <img class="profile-picture rounded m-r-15" src="<?= Core\Core::getCurrentUser()->getProfileImg() ?>" alt="">
                                <div>
                                    <p><?= Core\Core::translate('user.trips', 'publishedOn') ?>  <?= strftime("%d/%m/%Y", $trip->getCreationDate()->getTimestamp()) ?></p>
                                    <p><?= count(${'tripsSteps' . $trip->getId()}) ?> <?= (count(${'tripsSteps' . $trip->getId()}) > 1 ? "étapes" : "étape") ?></p>
                                </div>
                                <div class="m-l-auto">
                                    <a class="btn btn-primary btn-icon-only" href="{% url trip.page %}<?= '?trip_id=' . $trip->getId() ?>"><i class="material-icons">remove_red_eye</i></a>
                                    <a class="btn btn-success btn-icon-only" href="{% url user.show_edit_trip_form %}<?= '?trip_id=' . $trip->getId() ?>"><i class="material-icons">edit</i></a>
                                    <a class="btn btn-danger btn-icon-only click-to-open" data-modal="<?= 'modal-delete-trip-' . $trip->getId(); ?>"><i class="material-icons">delete</i></a>
                                </div>
                                <img class="m-t-10" src="<?= $trip->getImagePath() ?>">
                                <h1 class="card-title m-t-10"><?= $trip->getTitle() ?></h1>
                                <p class="card-text"><?= $trip->getExcerpt() ?></p>
                            </div>
                        </div>
                        <div id="<?= 'modal-delete-trip-' . $trip->getId(); ?>" class="modal">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <p class="text-subtitle"><?= Core\Core::translate('user.trips', 'confirmDeletion') ?></p>
                                    <span class="close close-modal">&times;</span>
                                </div>
                                <div class="modal-body">
                                    <p><?= Core\Core::translate('user.trips', 'sureToDeleteTrip') ?></p>
                                </div>
                                <div class="modal-footer">
                                    <a class="btn btn-danger cancel"><?= Core\Core::translate('user.trips', 'non') ?></a>
                                    <form method="POST" action="{% url user.delete_trip %}">
                                        <input type="hidden" name="trip-id" value="<?= $trip->getId(); ?>" />
                                        <button class="btn btn-success" type="submit"><?= Core\Core::translate('user.trips', 'ok') ?></a>
                                    </form>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </section>
            </div>
        </div>
    </div>
</main>
{% import templates.front.footer %}
{% endblock content %}