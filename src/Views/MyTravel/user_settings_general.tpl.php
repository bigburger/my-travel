{% import templates.front.front %}
{% block content %}
<main>
    <section id="cover">
        <div class="search-travel-bg-img bg-image-text vh-30">
            <img class="logo-small" src="/assets/images/logos/logo-secondary.svg" alt="Logo">
        </div>
    </section>
    <section id="user-settings">
        <div class="container">
            <div class="row">
                <div class="col-2 offset-2 col-xl-8 offset-xl-2">
                    <div class="card">
                        <nav>
                            <ul>
                                <li class="active"><a href="{% url user.settings.general %}" class="nav-setting"><?= Core\Core::translate('user.settings_general', 'personalInformations') ?></a></li>
                                <li><a href="{% url user.settings.password %}" class="nav-setting"><?= Core\Core::translate('user.settings_general', 'password') ?></a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-6 col-xl-8 offset-xl-2">
                    <div class="card">
                        <div class="card-body">
                            <h6 class="heading-6"><?= Core\Core::translate('user.settings_general', 'personalInformations') ?></h6>
                            <div class="divider m-y-16"></div>

                            <?php if ($successMessage) : ?>
                                <div class="alert alert-success alert-bottom-right">
                                <p><?= Core\Core::translate('user.settings_general', $successMessage); ?></p>
                                    <span class="close-alert">&times;</span>
                                </div>
                            <?php endif; ?>
                            <?php if ($errorMessage) : ?>
                                <div class="alert alert-error alert-bottom-right">
                                    <p><?= Core\Core::translate('user.settings_general', $errorMessage); ?></p>
                                    <span class="close-alert">&times;</span>
                                </div>
                            <?php endif; ?>

                            <form action="{% url <?= $settingsGeneralForm["action"]?> %}" method="<?= $settingsGeneralForm["method"]?>" id="<?= $settingsGeneralForm["id"]?>" enctype="<?= $settingsGeneralForm["enctype"]?>">
                                <?php foreach ($settingsGeneralForm['fields'] as $fieldName => $field) : ?>
                                    <?php if ($field['type'] === 'file') : ?>
                                        <div class="d-flex flex-column">
                                        <label class="m-b-10"><?= Core\Core::translate('user.settings_general', $field['other']['label']); ?></label>
                                            <img id="avatar-preview" class="preview avatar m-b-10" src="<?= Core\Core::getCurrentUser()->getProfileImg() ?>" alt="avatar">
                                            <div class="m-b-20">
                                                <input data-preview="avatar-preview" class="type-file d-none" type="file" name="<?=$field["name"]?>" id="<?=$field["id"]?>" accept="<?=$field["other"]['accept']?>">
                                                <label class="btn btn btn-success" for="<?=$field["name"]?>" class="m-b-10">
                                                    <span><?= Core\Core::translate('user.settings_general', $field['other']['buttonLabel']) ?></span>
                                                </label>
                                            </div>
                                        </div>
                                    <?php elseif ($field['type'] === 'textarea') : ?>
                                        <div class="form-group">
                                            <textarea class="<?=$field["class"]?>" name="<?=$field["name"]?>" id="<?=$field["id"]?>" rows="<?=$field["other"]['rows']?>"><?= $$fieldName ?></textarea>
                                            <label class="placeholder" for="<?=$field["name"]?>"><?=$field["other"]["label"]?></label>
                                        </div>
                                    <?php else : ?>
                                        <div class="form-group">
                                            <input class="<?=$field["class"]?>" type="<?=$field["type"]?>" name="<?=$field["name"]?>" id="<?=$field["id"]?>" value="<?= $$fieldName ?>"/>
                                            <label class="placeholder" for="<?=$field['name']; ?>"><?= empty($field['other']['label']) ? '' : Core\Core::translate('user.settings_general', $field['other']['label']); ?></label>
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </form>
                            <button class="btn btn-primary show-alert" type="submit" form="<?= $settingsGeneralForm["id"]?>" value="Enregistrer"><?= Core\Core::translate('user.settings_general', 'save') ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
{% import templates.front.footer %}
{% endblock content %}