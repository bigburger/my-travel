{% import templates.front.front %}

{% block content %}
<main>
    <section id="trip-steps">
        <div class="search-travel-bg-img bg-image-text vh-40"></div>
        <div class="container">
            <?php if ($successMessage) : ?>
                <div class="alert alert-success alert-bottom-right">
                    <p><?= Core\Core::translate('message.success', $successMessage); ?></p> 
                    <span class="close-alert">&times;</span>
                </div>
            <?php endif; ?>
            <div class="row text-center">
                <div class="col-12">
                    <h1 class="heading-4 trip-title"><?= $trip->getTitle() ?></h1>
                </div>
                <div class="divider col-5"></div>
                <div class="col-12">
                    <p class="text-grey text-paragraph"><?= Core\Core::translate('trip.comment', 'start')  . strftime("%d/%m/%Y", $trip->getStartingDate()->getTimestamp()) ?> <span class="m-x-10">-</span> <?= !empty($trip->getEndingDate()) ? Core\Core::translate('trip.comment', 'end') . strftime("%d/%m/%Y", $trip->getEndingDate()->getTimestamp()) : 'Fin: Non définie' ?></p>
                </div>
            </div>
            <div class="row m-t-25">   
                <div class="col-8 offset-2">
                    <p class="text-justify"><?= $trip->getDescription() ?></p>
                    <img class="card" src="<?= $trip->getImagePath() ? $trip->getImagePath() : '/assets/images/img-1.jpeg' ?>" alt="">
                </div>
            </div>
            <?php foreach($steps as $key => $step): ?>
                <div class="row">
                    <div class="col-8 offset-2">
                        <h2 class="heading-6 step-title"><?= $step->getTitle() ?></h2>
                        <p class="text-justify"><?= $step->getDescription() ?></p>
                    </div>
                    <div class="col-8 offset-2">
                        <img
                            data-modal="modal-img-step"
                            class="card click-to-open"
                            src=<?= $step->getImagePath() ?>
                            alt=""
                        >
                        <div id="modal-img-step" class="modal text-center">
                            <span class="close cancel close-modal-img">&times;</span>
                            <img class="modal-content modal-content-img">
                        </div>
                    </div>
                    <?php if ($step->getLongitude()): ?>
                        <div class="col-8 offset-2">
                            <div style="height: 300px;" class="card" id="<?= 'step-position-map-' . $step->getId() ?>"></div>
                        </div>
                        <script>
                            document.addEventListener('DOMContentLoaded', () => {
                                if (document.getElementById('step-position-map-' + <?= $step->getId() ?>)) {
                                    let stepPositionMap = L.map('step-position-map-' + <?= $step->getId() ?> ).setView([<?= $step->getLatitude() ?>, <?= $step->getLongitude() ?>], 8);
                                    if (stepPositionMap) {
                                        L.tileLayer('https://api.tiles.mapbox.com/v4/mapbox.run-bike-hike/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFsZWtvcyIsImEiOiJjancyMWtuYTgwMmJqNDhxbjJseTd2YmlpIn0.lIqOI3DJRryq2498wCYdtw', {
                                            maxZoom: 18,
                                            id: 'mapbox.streets',
                                            accessToken: 'pk.eyJ1IjoibWFsZWtvcyIsImEiOiJjancyMWtuYTgwMmJqNDhxbjJseTd2YmlpIn0.lIqOI3DJRryq2498wCYdtw'
                                        }).addTo(stepPositionMap);
                                        L.marker([<?= $step->getLatitude() ?>, <?= $step->getLongitude() ?>]).addTo(stepPositionMap);
                                    }
                                }
                            });
                        </script>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
    </section>
    <?php if (count($usedTags) > 0): ?>
        <section id="trip-tags">
            <div class="container">
                <div class="row">
                    <div class="m-t-40 col-8 offset-2">
                        <span class="m-r-5 text-subtitle">Tags :</span>
                        <span>
                            <?php foreach ($usedTags as $usedTag) : ?>
                                <a href="{% url trip.search %}<?= '?tag_id=' .$usedTag->getId() ?>">
                                    <span class="badge badge-primary badge-pill badge-clickable"><?= $usedTag->getLabel() ?></span>
                                </a>
                            <?php endforeach; ?>
                        </span>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>
    <section id="trip-author">
        <div class="container">
            <div class="row">
                <div class="col-8 offset-2">
                    <div class="divider m-y-40"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-8 offset-2">
                    <div class="author-details">
                        <div class="author-avatar">
                            <img class="author-avatar-description rounded" src="<?= $author->getProfileImg() ?>" alt="">
                        </div>
                        <div class="author-description">
                            <h2 class="text-subtitle"><?= $author->getUsername() ?></h2>
                            <p><?= $author->getDescription() ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="trip-comments">
        <div class="container">
            <div class="row">
                <div class="col-8 offset-2">
                    <div class="divider m-y-40"></div>
                </div>
                <div class="col-8 offset-2">
                    <h2 class="heading-6 text-center"><?= count($comments) . " " . (count($comments) > 1 ? Core\Core::translate('trip.comment', 'comments') : Core\Core::translate('trip.comment', 'comment')) ?></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-8 offset-2">
                    <?php foreach (array_reverse($comments) as $key => $comment) : ?>
                        <?php $commentAuthor = App\Repository\UsersRepository::getOneUserBy([["id", "=", $comment->getUserId()]]); ?>
                        <div class="m-t-30">
                            <div class="author-details">
                                <div class="author-avatar">
                                    <img class="author-avatar-comment rounded" src="<?= $commentAuthor->getProfileImg() ?>" alt="">
                                </div>
                                <div class="author-comment w-100">
                                    <div class="m-b-10">
                                        <span><?= $commentAuthor->getUsername() ?></span>
                                        · <span class="comment-date"><?= strftime("%d/%m/%Y à %H:%M", $comment->getCreationDate()->getTimestamp()) ?></span>
                                        <?php if ($comment->getUserId() == Core\Core::getCurrentUser()->getId()): ?>
                                            <span id="comment-actions-<?= $comment->getId() ?>">
                                                · <a id="update-comment-<?= $comment->getId() ?>" class="initial-color-link"><?= Core\Core::translate('trip.comment', 'edit') ?></a> -
                                                <a class="initial-color-link click-to-open" data-modal="<?= 'modal-delete-comment-' . $comment->getId(); ?>"><?= Core\Core::translate('trip.comment', 'delete') ?></a>
                                                <script>
                                                    var updateCommentLink = document.getElementById('update-comment-' + <?= $comment->getId() ?>);
                                                    if (updateCommentLink) {
                                                        updateCommentLink.addEventListener('click', () => {
                                                            var updateCommentForm = document.getElementById('update-comment-form-' + <?= $comment->getId() ?>);
                                                            var cancelCommentUpdateLink = document.getElementById('cancel-comment-update-' + <?= $comment->getId() ?>);
                                                            var commentActions = document.getElementById('comment-actions-' + <?= $comment->getId() ?>);
                                                            var comment = document.getElementById('comment-' + <?= $comment->getId() ?>);
                                                            var updatedCommentInput = document.getElementById('updated-comment-' + <?= $comment->getId() ?>);

                                                            updateCommentForm.style.display = 'block';
                                                            commentActions.style.display = 'none';
                                                            comment.style.display = 'none';
                                                            updatedCommentInput.value = "<?= strip_tags(htmlspecialchars_decode($comment->getText())) ?>";

                                                            cancelCommentUpdateLink.addEventListener('click', () => {
                                                                updateCommentForm.style.display = 'none';
                                                                commentActions.style.display = 'inline';
                                                                comment.style.display = 'block';
                                                            });
                                                        });
                                                    }
                                                </script>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                    <p id="comment-<?= $comment->getId() ?>"><?= $comment->getText() ?></p>
                                    <?php if ($comment->getUserId() == Core\Core::getCurrentUser()->getId()): ?>
                                        <form style="display: none;" class="w-100" method="POST" action="{% url trip.update_comment %}" id="update-comment-form-<?= $comment->getId() ?>">
                                            <input class="form-control w-100" type="text" name="updated-comment-<?= $comment->getId() ?>" id="updated-comment-<?= $comment->getId() ?>"/>
                                            <input type="hidden" name="comment-id" value="<?= $comment->getId(); ?>" />
                                            <input type="hidden" name="trip-id" value="<?= $trip->getId(); ?>" />
                                            <a id="cancel-comment-update-<?=$comment->getId() ?>" class="btn btn-danger"><?= Core\Core::translate('trip.comment', 'cancel') ?></a>
                                            <button class="btn btn-primary" type="submit"><?= Core\Core::translate('trip.comment', 'validate') ?></a>
                                        </form>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div id="<?= 'modal-delete-comment-' . $comment->getId(); ?>" class="modal">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <p class="text-subtitle"><?= Core\Core::translate('trip.comment', 'confirmDeletion') ?></p>
                                    <span class="close close-modal">&times;</span>
                                </div>
                                <div class="modal-body">
                                    <p><?= Core\Core::translate('trip.comment', 'sureDeleteComment') ?></p>
                                </div>
                                <div class="modal-footer">
                                    <a class="btn btn-danger cancel">Non</a>
                                    <form method="POST" action="{% url trip.delete_comment %}">
                                        <input type="hidden" name="comment-id" value="<?= $comment->getId(); ?>" />
                                        <input type="hidden" name="trip-id" value="<?= $trip->getId(); ?>" />
                                        <button class="btn btn-success" type="submit">Oui</a>
                                    </form>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>
    <section id="post-comment" class="m-b-30">
        <div class="container">
            <div class="row">
                <div class="col-8 offset-2">
                    <h2 class="heading-6 text-center m-t-75"><?= Core\Core::translate('trip.comment', 'leaveCommen') ?></h2>
                </div>
                <div class="col-8 offset-2">
                    <form action="{% url trip.page %}<?= "?trip_id=".$trip->getId() ?>" method="POST" id="post-comment">
                        <div class="post-comment">
                            <div class="form-group">
                                <textarea class="form-control w-100" name="comment" id="comment" rows="10" required></textarea>
                                <label class="placeholder" for="comment"><?= Core\Core::translate('trip.comment', 'leaveCommen') ?></label>
                            </div>
                        </div>
                        <div class="d-flex-end">
                            <button class="btn btn-primary btn-rounded" type="submit"><?= Core\Core::translate('trip.comment', 'toSend') ?></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main>
{% endblock content %}
{% import templates.front.footer %}