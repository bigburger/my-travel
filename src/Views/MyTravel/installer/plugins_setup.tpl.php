{% import templates.installer.installer %}

{% block content %}
<main>
    <div id="installer" class="plugins-setup-bg-img bg-image-text vh-100">
        <div class="card card-form">
            <div class="card-body">
                <div class="text-center">
                    <img class="logo-medium" src="/assets/images/logos/logo-primary.svg" alt="">
                    <h2><?= \Core\Core::translate('installer', 'installation') ?></h2>
                    <p class="text-subtitle m-b-10"><?= \Core\Core::translate('installer', 'plugins.title') ?></p>
                    <p class="m-b-10"><?= \Core\Core::translate('installer', 'plugins.introduction') ?></p>
                    <a href="https://www.google.com/recaptcha/" alt="Google Recaptcha 3" target="_blank"><?= \Core\Core::translate('installer', 'plugins.getRecaptchaKeys') ?></a>
                    <a href="https://www.tiny.cloud/signup/" alt="Tiny MCE" target="_blank"><?= \Core\Core::translate('installer', 'plugins.getTinyMCEKey') ?></a>
                    <?php if ($errorMessage) : ?>
                        <p class="error-message text-center text-danger m-t-10">
                            <?= $errorMessage ?>
                        </p>
                    <?php endif; ?>
                </div>
                <form action="{% url <?=$pluginsSetupForm["action"]?> %}" method="<?=$pluginsSetupForm["method"]?>" id="<?=$pluginsSetupForm["id"]?>">
                    <?php foreach ($pluginsSetupForm['fields'] as $fieldName => $field) : ?>
                        <div class="form-group">
                            <input class="<?=$field["class"]?>" type="<?=$field["type"]?>" name="<?=$field["name"]?>" value="<?= $form_params[$field["name"]] ?? '' ?>"
                                    id="<?=$field["id"]?>" <?=array_keys($field, "required")[0]?>/>
                            <label class="placeholder placeholder-icon" for="<?=$field["name"]?>"><?= \Core\Core::translate('installer.plugins_setup', $field["other"]["label"]) ?></label>
                        </div>
                    <?php endforeach; ?>
                </form>
            </div>
            <div class="card-footer">
                <button class="btn btn-rounded btn-primary" form="plugins-setup"><?= \Core\Core::translate('installer', 'plugins.nextButton') ?></button>
            </div>
        </div>
    </div>
</main>
{% endblock content %}