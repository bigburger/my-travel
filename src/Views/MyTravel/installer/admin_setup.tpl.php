{% import templates.installer.installer %}

{% block content %}
<main>
    <div id="installer" class="admin-setup-bg-img bg-image-text vh-100">
        <div class="card card-form">
            <div class="card-body">
                <div class="text-center">
                    <img class="logo-medium" src="/assets/images/logos/logo-primary.svg" alt="">
                    <h2><?= \Core\Core::translate('installer', 'installation') ?></h2>
                    <p class="text-subtitle m-b-10"><?= \Core\Core::translate('installer', 'admin.title') ?></p>
                    <p><?= \Core\Core::translate('installer', 'admin.introduction') ?></p>
                    <?php if ($errorMessage) : ?>
                        <p class="error-message text-center text-danger m-t-10">
                            <?= $errorMessage ?>
                        </p>
                    <?php endif; ?>
                </div>
                <form action="{% url <?=$adminSetupForm["action"]?> %}" method="<?=$adminSetupForm["method"]?>" id="<?=$adminSetupForm["id"]?>">
                    <?php foreach ($adminSetupForm['fields'] as $fieldName => $field) : ?>
                        <div class="<?=implode(' ', $field["other"]["inputIconClass"])?>">
                            <input class="<?=$field["class"]?>" type="<?=$field["type"]?>" name="<?=$field["name"]?>" value="<?= $form_params[$field["name"]] ?? '' ?>" id="<?=$field["id"]?>" <?=array_keys($field, "required")[0]?>/>
                            <label class="placeholder" for="<?=$field["name"]?>"><?= \Core\Core::translate('installer.admin_setup', $field["other"]["label"]) ?></label>
                        </div>
                    <?php endforeach; ?>
                </form>
            </div>
            <div class="card-footer">
                <button class="btn btn-rounded btn-primary" form="<?=$adminSetupForm["id"]?>"><?= \Core\Core::translate('installer', 'admin.nextButton') ?></button>
            </div>
        </div>
    </div>
</main>
{% endblock content %}