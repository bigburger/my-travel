{% import templates.installer.installer %}

{% block content %}
<main>
    <div id="installer" class="signin-bg-img bg-image-text vh-100">
        <div class="card card-form">
            <div class="card-body">
                <div class="text-center">
                    <img class="logo-medium" src="/assets/images/logos/logo-primary.svg" alt="">
                    <h2>MyTravel</h2>
                    <p class="text-subtitle"><?= \Core\Core::translate('installer', 'installation') ?></p>
                </div>
            </div>
            <div class="card-footer">
                <p class="m-b-10"><?= \Core\Core::translate('installer', 'ipNotAllowed.yourIp') ?><?= $ip ?></p>
                <p class="m-b-10"><?= \Core\Core::translate('installer', 'ipNotAllowed.introduction') ?></p>
                <p><?= \Core\Core::translate('installer', 'ipNotAllowed.copyIp') ?></p>
            </div>
        </div>
    </div>
</main>
{% endblock content %}