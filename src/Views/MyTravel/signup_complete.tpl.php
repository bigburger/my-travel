{% import templates.front.front %}

{% block content %}
<main>
    <div id="signin" class="signup-bg-img bg-image-text vh-100">
        <div>
            <div class="card">
                <div class="card-body">
                    <div class="text-center m-b-10">
                        <img class="logo-medium" src="/assets/images/logos/logo-primary.svg" alt="">
                        <p class="text-subtitle m-b-10"><?= Core\Core::translate('signup.complete', 'inscriptionFinalisation') ?></p>
                    </div>
                    <?php if ($errorMessage) : ?>
                        <p class="error-message text-center text-danger">
                            <?= $errorMessage ?>
                        </p>
                    <?php elseif ($successMessage) : ?>
                        <p class="success-message text-center text-success">
                            <?= $successMessage ?>
                        </p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</main>
{% endblock content %}