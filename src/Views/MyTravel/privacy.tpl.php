<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>My Travel</title>
        <meta name=‘viewport’ content=‘width=device-width, initial-scale=1.0, user-scalable=« no"’>
        <link rel="stylesheet" type="text/css" href="/assets/style/css/main.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="icon" href="/assets/images/icons/logo-icon.ico" type="image/x-icon" />
    </head>
    <body>
        <header>
            <nav class="navbar fixed-top bg-primary">
                <a href="#" class="navbar-brand">My Travel</a>
                <div class="navbar-left">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="#" class="nav-link">Accueil</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">Gallerie</a>
                        </li>
                    </ul>
                </div>
                <div class="navbar-right d-flex-end">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="#" class="nav-link">Inscription</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">Connexion</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <main class="p-t-130">
            <div class="container">
                <div class="row">
                    <div class="col-3">
                        <div class="left-menu">
                            <ul>
                                <li><a href="">Présentation du site Internet</a></li>
                                <li><a href="">Conditions générales d’utilisation du site et des services proposés</a></li>
                                <li><a href="">Description des services fournis</a></li>
                                <li><a href="">Limitations contractuelles sur les données techniques</a></li>
                                <li><a href="">Propriété intellectuelle et contrefaçons</a></li>
                                <li><a href="">Limitations de responsabilité</a></li>
                                <li><a href="">Gestion des données personnelles</a></li>
                                <li><a href="">Responsables de la collecte des données personnelles</a></li>
                                <li><a href="">Finalité des données collectées</a></li>
                                <li><a href="">Droit d’accès, de rectification et d’opposition</a></li>
                                <li><a href="">Non-communication des données personnelles</a></li>
                                <li><a href="">Notification d’incident</a></li>
                                <li><a href="">Liens hypertextes « cookies » et balises (“tags”) internet</a></li>
                                <li><a href="">Droit applicable et attribution de juridiction</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-9">
                        <div class="right-side">
                            <h6 class="text-subtitle m-t-0">Présentation du site internet</h6>
                            <p class="text-paragraph">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos amet aperiam earum consequatur commodi molestias et dolorem non nam sapiente nemo, nostrum optio deleniti! Consequatur earum numquam magnam sit officia?
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos amet aperiam earum consequatur commodi molestias et dolorem non nam sapiente nemo, nostrum optio deleniti! Consequatur earum numquam magnam sit officia?
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos amet aperiam earum consequatur commodi molestias et dolorem non nam sapiente nemo, nostrum optio deleniti! Consequatur earum numquam magnam sit officia?
                            </p>
                            <h6 class="text-subtitle">Présentation du site internet</h6>
                            <p class="text-paragraph">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos amet aperiam earum consequatur commodi molestias et dolorem non nam sapiente nemo, nostrum optio deleniti! Consequatur earum numquam magnam sit officia?
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos amet aperiam earum consequatur commodi molestias et dolorem non nam sapiente nemo, nostrum optio deleniti! Consequatur earum numquam magnam sit officia?
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos amet aperiam earum consequatur commodi molestias et dolorem non nam sapiente nemo, nostrum optio deleniti! Consequatur earum numquam magnam sit officia?
                            </p>
                            <h6 class="text-subtitle">Présentation du site internet</h6>
                            <p class="text-paragraph">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos amet aperiam earum consequatur commodi molestias et dolorem non nam sapiente nemo, nostrum optio deleniti! Consequatur earum numquam magnam sit officia?
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos amet aperiam earum consequatur commodi molestias et dolorem non nam sapiente nemo, nostrum optio deleniti! Consequatur earum numquam magnam sit officia?
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos amet aperiam earum consequatur commodi molestias et dolorem non nam sapiente nemo, nostrum optio deleniti! Consequatur earum numquam magnam sit officia?
                            </p>
                            <h6 class="text-subtitle">Présentation du site internet</h6>
                            <p class="text-paragraph">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos amet aperiam earum consequatur commodi molestias et dolorem non nam sapiente nemo, nostrum optio deleniti! Consequatur earum numquam magnam sit officia?
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos amet aperiam earum consequatur commodi molestias et dolorem non nam sapiente nemo, nostrum optio deleniti! Consequatur earum numquam magnam sit officia?
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos amet aperiam earum consequatur commodi molestias et dolorem non nam sapiente nemo, nostrum optio deleniti! Consequatur earum numquam magnam sit officia?
                            </p>
                            <h6 class="text-subtitle">Présentation du site internet</h6>
                            <p class="text-paragraph">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos amet aperiam earum consequatur commodi molestias et dolorem non nam sapiente nemo, nostrum optio deleniti! Consequatur earum numquam magnam sit officia?
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos amet aperiam earum consequatur commodi molestias et dolorem non nam sapiente nemo, nostrum optio deleniti! Consequatur earum numquam magnam sit officia?
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos amet aperiam earum consequatur commodi molestias et dolorem non nam sapiente nemo, nostrum optio deleniti! Consequatur earum numquam magnam sit officia?
                            </p>
                            <h6 class="text-subtitle">Présentation du site internet</h6>
                            <p class="text-paragraph">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos amet aperiam earum consequatur commodi molestias et dolorem non nam sapiente nemo, nostrum optio deleniti! Consequatur earum numquam magnam sit officia?
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos amet aperiam earum consequatur commodi molestias et dolorem non nam sapiente nemo, nostrum optio deleniti! Consequatur earum numquam magnam sit officia?
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos amet aperiam earum consequatur commodi molestias et dolorem non nam sapiente nemo, nostrum optio deleniti! Consequatur earum numquam magnam sit officia?
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        {% import templates.front.footer %}
    </body>
</html>
