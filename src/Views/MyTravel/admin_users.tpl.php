{% import templates.back.back %}
{% block content %}
<main>
    {% import templates.back.sidenav %}
    <div class="p-l-200 p-t-80">
        <section id="users-list">
            <div class="row">
                <div class="col-12">
                    <h6 class="heading-6 m-t-20"><?= Core\Core::translate('admin.users', 'usersList'); ?></h6>
                </div>
            </div>
            <?php if ($successMessage) : ?>
                <div class="alert alert-success alert-bottom-right">
                    <p><?= $successMessage ?></p>
                    <span class="close-alert">&times;</span>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="add-user">
                                <form action="{% url back.users %}" method="POST">
                                    <div class="form-group m-t-5 m-r-20">
                                        <input
                                        class="form-control-default form-control-default-icon text-paragraph search-icon"
                                        placeholder="<?= Core\Core::translate('admin.users', 'search'); ?>"
                                        type="text"
                                        name="searchQuery"
                                        id="searchQuery"
                                        value="<?= (isset($searchQuery) ? $searchQuery : null); ?>"
                                        >
                                    </div>
                                </form>
                            </div>
                            <div class="x-scroll">
                                <table class="table table-rounded">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th><?= Core\Core::translate('admin.users', 'lastName'); ?></th>
                                            <th><?= Core\Core::translate('admin.users', 'firstName'); ?></th>
                                            <th><?= Core\Core::translate('admin.users', 'username'); ?></th>
                                            <th><?= Core\Core::translate('admin.users', 'emailAdress'); ?></th>
                                            <th><?= Core\Core::translate('admin.users', 'roleShort'); ?></th>
                                            <th><?= Core\Core::translate('admin.users', 'statusShort'); ?></th>
                                            <th><?= Core\Core::translate('admin.users', 'creationDate'); ?></th>
                                            <th><?= Core\Core::translate('admin.users', 'actions'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($users as $key => $user): ?>
                                            <tr>
                                                <td><?= $user->getId(); ?></td>
                                                <td><?= $user->getLastname(); ?></td>
                                                <td><?= $user->getFirstname(); ?></td>
                                                <td><?= $user->getUsername(); ?></td>
                                                <td><?= $user->getEmail(); ?></td>
                                                <td><?= $user->getRole(); ?></td>
                                                <td><?= $user->getStatus() == 1 ?  Core\Core::translate('admin.users', 'activated')  : Core\Core::translate('admin.users', 'disabled'); ?></td>
                                                <td><?= strftime('%d/%m/%Y', $user->getCreationDate()->getTimestamp()); ?></td>
                                                <td>
                                                    <a class="btn btn-success btn-icon-only click-to-open" data-modal="<?= 'modal-edit-user-'.$user->getId(); ?>"><i class="material-icons">edit</i></a>
                                                    <?php if ($user->getId() != \Core\Core::getCurrentUser()->getId()): ?>
                                                        <a class="btn btn-danger btn-icon-only click-to-open" data-modal="<?= 'modal-delete-user-'.$user->getId(); ?>"><i class="material-icons">delete</i></a>
                                                    <?php endif; ?>
                                                    <div id="<?= 'modal-edit-user-'.$user->getId(); ?>" class="modal">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <p class="text-subtitle"><?= Core\Core::translate('admin.users', 'fullProfileUser'); ?></p>
                                                                <span class="close close-modal">&times;</span>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="{% url back.user_update %}" method="POST" id="edit-user-form-<?= $user->getId() ?>">
                                                                    <div class="form-group">
                                                                        <input class="form-control w-100" type="text" name="lastname" id="lastname" value="<?= $user->getLastname(); ?>">
                                                                        <label class="placeholder" for="lastname"><?= Core\Core::translate('admin.users', 'lastName'); ?></label>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <input class="form-control w-100" type="text" name="firstname" id="firstname" value="<?= $user->getFirstname(); ?>">
                                                                        <label class="placeholder" for="firstname"><?= Core\Core::translate('admin.users', 'firstName'); ?></label>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <input class="form-control w-100" type="text" name="username" id="username" value="<?= $user->getUsername(); ?>">
                                                                        <label class="placeholder" fuser.delete="username"><?= Core\Core::translate('admin.users', 'username'); ?></label>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <input class="form-control w-100" type="email" name="email" id="email" value="<?= $user->getEmail(); ?>">
                                                                        <label class="placeholder" fuser.delete="email"><?= Core\Core::translate('admin.users', 'emailAdress'); ?></label>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label><?= Core\Core::translate('admin.users', 'role'); ?></label>
                                                                        <select id="role" name="role">
                                                                            <?php if (($user->getId() == \Core\Core::getCurrentUser()->getId() && \Core\Core::getCurrentUser()->getRole() == 'user') ||
                                                                            $user->getId() != \Core\Core::getCurrentUser()->getId()): ?>
                                                                                <option <?= $user->getRole() == 'user' ? 'selected' : ''; ?> value="user"><?= Core\Core::translate('admin.users', 'user'); ?></option>
                                                                            <?php endif; ?>
                                                                            <?php if (($user->getId() == \Core\Core::getCurrentUser()->getId() && \Core\Core::getCurrentUser()->getRole() == 'admin') ||
                                                                                $user->getId() != \Core\Core::getCurrentUser()->getId()): ?>
                                                                                <option <?= $user->getRole() == 'admin' ? 'selected' : ''; ?> value="admin"><?= Core\Core::translate('admin.users', 'administrator'); ?></option>
                                                                            <?php endif; ?>
                                                                        </select>
                                                                    </div>
                                                                    <?php if ($user->getId() != \Core\Core::getCurrentUser()->getId()): ?>
                                                                        <div class="form-group m-t-10">
                                                                            <label><?= Core\Core::translate('admin.users', 'status'); ?></label>
                                                                            <select id="status" name="status">
                                                                                <option <?= $user->getStatus() == 1 ? 'selected' : ''; ?> value="1"><?= Core\Core::translate('admin.users', 'activated'); ?></option>
                                                                                <option <?= $user->getStatus() == 0 ? 'selected' : ''; ?> value="0"><?= Core\Core::translate('admin.users', 'disabled'); ?></option>
                                                                            </select>
                                                                        </div>
                                                                    <?php else: ?>
                                                                        <input type="hidden" name="status" value="1" />
                                                                    <?php endif; ?>
                                                                    <div class="form-group">
                                                                        <textarea class="form-control w-100" name="description" id="description" rows="5"><?= $user->getDescription(); ?></textarea>
                                                                        <label class="placeholder" for="description"><?= Core\Core::translate('admin.users', 'description'); ?></label>
                                                                    </div>
                                                                    <input type="hidden" name="id" value="<?= $user->getId(); ?>" />
                                                                </form>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <a class="btn btn-danger cancel"><?= Core\Core::translate('admin.users', 'cancel'); ?></a>
                                                                <button class="btn btn-success" type="submit" form="edit-user-form-<?= $user->getId() ?>"><?= Core\Core::translate('admin.users', 'validate'); ?></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="<?= 'modal-delete-user-'.$user->getId(); ?>" class="modal">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <p class="text-subtitle"><?= Core\Core::translate('admin.users', 'confirmDeletion'); ?></p>
                                                                <span class="close close-modal">&times;</span>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p><?= Core\Core::translate('admin.users', 'sureToDelete'); ?></p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <a class="btn btn-danger cancel"><?= Core\Core::translate('admin.users', 'noButton'); ?></a>
                                                                <form method="POST" action="{% url back.user_delete %}">
                                                                    <input type="hidden" name="user-id" value="<?= $user->getId(); ?>" />
                                                                    <button class="btn btn-success" type="submit"><?= Core\Core::translate('admin.users', 'yesButton'); ?></a>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
{% endblock content %}