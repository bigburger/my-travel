{% import templates.back.back %}
{% block content %}
<main>
    {% import templates.back.sidenav %}
    <div class="p-l-200 p-t-80">
        <section id="general-settings" class="m-t-20">
            <div class="row">
                <div class="col-12">
                    <?php if ($successMessage) : ?>
                        <div class="alert alert-success alert-bottom-right">
                            <p><?= $successMessage ?></p>
                            <span class="close-alert">&times;</span>
                        </div>
                    <?php endif; ?>
                    <?php if ($errorMessage) : ?>
                        <div class="alert alert-error alert-bottom-right">
                            <p><?= $errorMessage ?></p>
                            <span class="close-alert">&times;</span>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <h6 class="heading-6"><?= Core\Core::translate('admin.setting', 'generalSettings'); ?></h6>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <form action="{% url <?= $generalSettingsForm["action"]?> %}" method="<?= $generalSettingsForm["method"]?>" id="<?= $generalSettingsForm["id"]?>">
                        <?php foreach ($generalSettingsForm['fields'] as $fieldName => $field) : ?>
                            <?php if ($field['type'] === 'select') : ?>
                                <div class="form-group">
                                    <label class="form-control-default-label" for="<?=$field["name"]?>"><?= empty($field['other']['label']) ? '' : Core\Core::translate('admin.setting', $field['other']['label']); ?></label>
                                    <select class="<?=$field["class"]?>" name="<?=$field["name"]?>" id="<?=$field["id"]?>">
                                        <?php foreach ($field["options"] as $option) : ?>
                                            <option value="<?= $option['value'] ?>"
                                                    <?php if ($option['value'] === $site_language || $option['value'] === \Core\Core::getConfig('signup.sendMail')) : ?>
                                                        selected
                                                    <?php endif; ?>
                                            >
                                            <?= $option['desc'] ?>
                                        </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            <?php else : ?>
                                <div class="form-group">
                                    <label class="form-control-default-label" for="<?=$field["name"]?>"><?= empty($field['other']['label']) ? '' : Core\Core::translate('admin.setting', $field['other']['label']); ?></label>
                                    <input class="<?=$field["class"]?>" type="<?=$field["type"]?>" name="<?=$field["name"]?>" id="<?=$field["id"]?>" value="<?= $$fieldName ?>"/>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </form>
                    <div class="m-t-30">
                        <button class="btn btn-primary" type="submit" form="<?= $generalSettingsForm["id"]?>"><?= Core\Core::translate('admin.setting', 'save'); ?></button>
                    </div>
                </div>
            </div>
        </section>
        <section id="social-networks-settings" class="m-t-20">
            <div class="row">
                <div class="col-12">
                    <?php if ($successMessage) : ?>
                        <div class="alert alert-success alert-bottom-right">
                            <p><?= $successMessage ?></p>
                            <span class="close-alert">&times;</span>
                        </div>
                    <?php endif; ?>
                    <?php if ($errorMessage) : ?>
                        <div class="alert alert-error alert-bottom-right">
                            <p><?= $errorMessage ?></p>
                            <span class="close-alert">&times;</span>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <h6 class="heading-6"><?= Core\Core::translate('admin.setting', 'socialNetworksSettings'); ?></h6>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <form action="{% url <?= $socialNetworksSettingsForm["action"]?> %}" method="<?= $socialNetworksSettingsForm["method"]?>" id="<?= $socialNetworksSettingsForm["id"]?>">
                        <?php foreach ($socialNetworksSettingsForm['fields'] as $fieldName => $field) : ?>
                            <div class="form-group">
                                <label class="form-control-default-label" for="<?=$field["name"]?>"><?= empty($field['other']['label']) ? '' : Core\Core::translate('admin.setting', $field['other']['label']); ?></label>
                                <input class="<?=$field["class"]?>" type="<?=$field["type"]?>" name="<?=$field["name"]?>" id="<?=$field["id"]?>" value="<?= $$fieldName ?>"/>
                            </div>
                        <?php endforeach; ?>
                    </form>
                    <div class="m-t-30">
                        <button class="btn btn-primary" type="submit" form="<?= $socialNetworksSettingsForm["id"]?>"><?= Core\Core::translate('admin.setting', 'save'); ?></button>
                    </div>
                </div>
            </div>
        </section>
        <section id="contact-settings" class="m-t-20">
            <div class="row">
                <div class="col-12">
                    <?php if ($successMessage) : ?>
                        <div class="alert alert-success alert-bottom-right">
                            <p><?= $successMessage ?></p>
                            <span class="close-alert">&times;</span>
                        </div>
                    <?php endif; ?>
                    <?php if ($errorMessage) : ?>
                        <div class="alert alert-error alert-bottom-right">
                            <p><?= $errorMessage ?></p>
                            <span class="close-alert">&times;</span>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <h6 class="heading-6"><?= Core\Core::translate('admin.setting', 'contactSettings'); ?></h6>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <form action="{% url <?= $contactSettingsForm["action"]?> %}" method="<?= $contactSettingsForm["method"]?>" id="<?= $contactSettingsForm["id"]?>">
                        <?php foreach ($contactSettingsForm['fields'] as $fieldName => $field) : ?>
                            <div class="form-group">
                                <label class="form-control-default-label" for="<?=$field["name"]?>"><?= empty($field['other']['label']) ? '' : Core\Core::translate('admin.setting', $field['other']['label']); ?></label>
                                <input class="<?=$field["class"]?>" type="<?=$field["type"]?>" name="<?=$field["name"]?>" id="<?=$field["id"]?>" value="<?= $$fieldName ?>"/>
                            </div>
                        <?php endforeach; ?>
                    </form>
                    <div class="m-t-30">
                        <button class="btn btn-primary" type="submit" form="<?= $contactSettingsForm["id"]?>"><?= Core\Core::translate('admin.setting', 'save'); ?></button>
                    </div>
                </div>
            </div>
        </section>
        <section id="graphic-settings" class="m-t-40">
            <div class="row">
                <div class="col-12">
                    <h6 class="heading-6"><?= Core\Core::translate('admin.setting', 'graphicalSettings'); ?></h6>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <form action="{% url <?= $graphicsSettingsForm["action"]?> %}" method="<?= $graphicsSettingsForm["method"]?>" id="<?= $graphicsSettingsForm["id"]?>" enctype="<?= $graphicsSettingsForm["enctype"]?>">
                        <?php foreach ($graphicsSettingsForm['fields'] as $fieldName => $field) : ?>
                            <?php if ($field['type'] === 'file') : ?>
                                <div class="d-flex m-y-20">
                                    <div class="d-flex">
                                        <label class="form-control-default-label"><?= empty($field['other']['label']) ? '' : Core\Core::translate('admin.setting', $field['other']['label']); ?></label>
                                        <img class="<?=$field["other"]["class"]?>" src="<?=$field["other"]['path']?>" alt="<?=$field["name"]?>">
                                        <div>
                                            <input class="type-file d-none" type="file" name="<?=$field["name"]?>" id="<?=$field["id"]?>" accept="<?=$field["other"]['accept']?>">
                                            <label class="btn btn btn-success m-t-30" for="<?=$field["name"]?>" class="m-b-10">
                                                <span><?= empty($field['other']['label']) ? '' : Core\Core::translate('admin.setting', $field['other']['buttonLabel']); ?></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            <?php else : ?>
                                <div class="form-group">
                                    <label class="form-control-default-label" for="<?=$field["name"]?>"><?= Core\Core::translate('admin.setting', 'themeName'); ?></label>
                                    <input class="<?=$field["class"]?>" type="<?=$field["type"]?>" name="<?=$field["name"]?>" id="<?=$field["id"]?>" value="<?= $$fieldName ?>"/>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </form>
                    <div class="m-t-40">
                        <button class="btn btn-primary" type="submit" form="<?= $graphicsSettingsForm["id"]?>"><?= Core\Core::translate('admin.setting', 'save'); ?></button>
                    </div>
                </div>
            </div>
        </section>
        <section id="plugin-settings" class="m-t-40">
            <div class="row">
                <div class="col-12">
                    <h6 class="heading-6"><?= Core\Core::translate('admin.setting', 'pluginsSettings'); ?></h6>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <form action="{% url <?= $pluginsSettingsForm["action"]?> %}" method="<?= $pluginsSettingsForm["method"]?>" id="<?= $pluginsSettingsForm["id"]?>">
                        <?php foreach ($pluginsSettingsForm['fields'] as $fieldName => $field) : ?>
                            <div class="form-group">
                                <label class="form-control-default-label" for="<?=$field["name"]?>"><?= empty($field['other']['label']) ? '' : Core\Core::translate('admin.setting', $field['other']['label']); ?></label>
                                <input class="<?=$field["class"]?>" type="<?=$field["type"]?>" name="<?=$field["name"]?>" id="<?=$field["id"]?>" value="<?= $$fieldName ?>"/>
                            </div>
                        <?php endforeach; ?>
                    </form>
                    <div class="m-t-40">
                        <button class="btn btn-primary" type="submit" form="<?= $pluginsSettingsForm["id"]?>"><?= Core\Core::translate('admin.setting', 'save'); ?></button>
                    </div>
                </div>
            </div>
        </section>
        <section id="database-settings" class="m-t-40">
            <div class="row">
                <div class="col-12">
                    <h6 class="heading-6"><?= Core\Core::translate('admin.setting', 'databaseSettings'); ?></h6>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <form action="{% url <?= $databaseSettingsForm["action"]?> %}" method="<?= $databaseSettingsForm["method"]?>" id="<?= $databaseSettingsForm["id"]?>">
                        <?php foreach ($databaseSettingsForm['fields'] as $fieldName => $field) : ?>
                            <div class="form-group">
                                <label class="form-control-default-label" for="<?=$field["name"]?>"><?= empty($field['other']['label']) ? '' : Core\Core::translate('admin.setting', $field['other']['label']); ?></label>
                                <input class="<?=$field["class"]?>" type="<?=$field["type"]?>" name="<?=$field["name"]?>" id="<?=$field["id"]?>" value="<?= $$fieldName ?>"/>
                            </div>
                        <?php endforeach; ?>
                    </form>
                    <div class="m-y-40">
                        <button class="btn btn-primary" type="submit" form="<?= $databaseSettingsForm["id"]?>"><?= Core\Core::translate('admin.setting', 'save'); ?></button>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
{% endblock content %}