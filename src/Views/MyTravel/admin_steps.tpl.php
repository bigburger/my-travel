{% import templates.back.back %}
{% block content %}
<main>
    {% import templates.back.sidenav %}
    <div class="p-l-200 p-t-80">
        <section id="users-list">
            <div class="row">
                <div class="col-12">
                    <h6 class="heading-6 m-t-20"><?= Core\Core::translate('admin.step', 'listStep'); ?></h6>
                </div>
            </div>
            <?php if ($successMessage) : ?>
                <div class="alert alert-success alert-bottom-right">
                    <p><?= $successMessage ?></p>
                    <span class="close-alert">&times;</span>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="add-user">
                                <form action="{% url back.steps %}" method="POST">
                                    <div class="form-group m-t-5 m-r-20">
                                        <input
                                        class="form-control-default form-control-default-icon text-paragraph search-icon"
                                        placeholder="<?= Core\Core::translate('admin.step', 'searchStep'); ?>"
                                        type="text"
                                        name="searchQuery"
                                        id="searchQuery"
                                        value="<?= (isset($searchQuery) ? $searchQuery : null); ?>"
                                        >
                                    </div>
                                </form>
                            </div>
                            <div class="x-scroll">
                                <table class="table table-rounded">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th><?= Core\Core::translate('admin.step', 'trip'); ?></th>
                                            <th><?= Core\Core::translate('admin.step', 'title'); ?></th>
                                            <th width="50%"><?= Core\Core::translate('admin.step', 'description'); ?></th>
                                            <th><?= Core\Core::translate('admin.step', 'startDate'); ?></th>
                                            <th><?= Core\Core::translate('admin.step', 'endDate'); ?></th>
                                            <th><?= Core\Core::translate('admin.users', 'creationDate'); ?></th>
                                            <th><?= Core\Core::translate('admin.step', 'action'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($steps as $key => $step): ?>
                                            <?php $trip = $tripsRepo->getOneTripBy([["id", "=", $step->getTripId()]]); ?> 
                                            <tr>
                                                <td><?= $step->getId() ?></td>
                                                <td width="10%"><?= $trip->getTitle() . ' (#' . $trip->getId() . ')' ?></td>
                                                <td><?= $step->getTitle() ?></td>
                                                <td><?= $step->getExcerpt() ?></td>
                                                <td><?= strftime('%d/%m/%Y', $step->getStartingDate()->getTimestamp()) ?></td>
                                                <td><?= strftime('%d/%m/%Y', $step->getEndingDate()->getTimestamp()) ?></td>
                                                <td width="10%"><?= strftime('%d/%m/%Y', $step->getCreationDate()->getTimestamp()) ?></td>
                                                <td width="5%">
                                                    <a class="btn btn-danger btn-icon-only click-to-open" data-modal="<?= 'modal-delete-step-'.$step->getId(); ?>"><i class="material-icons">delete</i></a>
                                                    <div id="<?= 'modal-delete-step-'.$step->getId(); ?>" class="modal">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <p class="text-subtitle"><?= Core\Core::translate('admin.step', 'confirmatioDeletion'); ?></p>
                                                                <span class="close close-modal">&times;</span>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p><?= Core\Core::translate('admin.step', 'sureToDeleteStep'); ?></p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <a class="btn btn-danger cancel"><?= Core\Core::translate('admin.step', 'non'); ?></a>
                                                                <form method="POST" action="{% url back.step_delete %}">
                                                                    <input type="hidden" name="step-id" value="<?= $step->getId(); ?>" />
                                                                    <button class="btn btn-success" type="submit"><?= Core\Core::translate('admin.step', 'ok'); ?></a>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
{% endblock content %}