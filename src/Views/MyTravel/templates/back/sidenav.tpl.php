<div class="sidenav fixed-left h-100">
    <a class="sidenav-nav" class="active" href="{% url back.home %}"><i class="material-icons">dashboard</i><?= Core\Core::translate('template.back', 'nav.dashboard') ?></a>
    <a class="sidenav-nav" href="{% url back.users %}"><i class="material-icons">people</i><?= Core\Core::translate('template.back', 'nav.users') ?></a>
    <a class="sidenav-nav" href="{% url back.comments %}"><i class="material-icons">comment</i><?= Core\Core::translate('template.back', 'nav.comments') ?></a>
    <a class="sidenav-nav" href="{% url back.trips %}"><i class="material-icons">flight</i><?= Core\Core::translate('template.back', 'nav.trips') ?></a>
    <a class="sidenav-nav" href="{% url back.steps %}"><i class="material-icons">place</i><?= Core\Core::translate('template.back', 'nav.steps') ?></a>
    <a class="sidenav-nav" href="{% url back.pages %}"><i class="material-icons">web</i><?= Core\Core::translate('template.back', 'nav.pages') ?></a>
    <a class="sidenav-nav" href="{% url back.site.settings %}"><i class="material-icons">settings</i><?= Core\Core::translate('template.back', 'nav.settings') ?></a>
</div>