<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= $site_name ?> - <?= Core\Core::translate('template.back', 'administration') ?></title>
        <link rel="stylesheet" type="text/css" href="/assets/<?= $theme_name ?>/style/css/main.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="icon" href="/assets/images/icons/logo-icon.ico" type="image/x-icon" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.js"></script>
        <script src="/assets/<?= $theme_name ?>/js/sidenav.js"></script>
        <script src="/assets/<?= $theme_name ?>/js/modal.js"></script>
        <script src="/assets/<?= $theme_name ?>/js/input.js"></script>
        <script src="/assets/<?= $theme_name ?>/js/alert.js"></script>
        <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=<?= $tinymce_key ?>"></script>
        <script>tinymce.init({ menubar: false, mode:"specific_textareas", editor_selector:'tiny-mce'});</script>
    </head>
    <body>
        {% import templates.back.header %}
        {{ block content }}
    </body>
</html>