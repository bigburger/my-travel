<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?= $site_name ?></title>
        <meta name=‘viewport’ content=‘width=device-width, initial-scale=1.0, user-scalable=« no"’>
        <link rel="stylesheet" type="text/css" href="/assets/<?= $theme_name ?>/style/css/main.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="icon" href="/assets/images/icons/logo-icon.ico" type="image/x-icon" />
        <script src="/assets/<?= $theme_name ?>/js/navbar.js"></script>
    </head>
    <body>
        {% import templates.front.header %}
        {{ block content }}
    </body>
</html>