{% import templates.front.front %}

{% block content %}
<main>
    <div id="signin" class="signin-bg-img bg-image-text vh-100">
        <div>
            <div class="card card-form">
                <div class="card-body">
                    <div class="text-center">
                        <img class="logo-medium" src="/assets/images/logos/logo-primary.svg" alt="">
                        <p class="text-subtitle"><?= Core\Core::translate('signin', 'signin') ?></p>
                    </div>
                    <?php if ($successMessage): ?>
                        <p class="m-t-10 success-message text-center text-success">
                            <?= $successMessage ?>
                        </p>
                    <?php endif; ?>
                    <?php if ($errorMessage): ?>
                        <p class="m-t-10 error-message text-center text-danger">
                            <?= $errorMessage ?>
                        </p>
                    <?php endif; ?>
                    <form action="{% url <?=$signinForm["action"]?> %}" method="<?=$signinForm["method"]?>" id="<?=$signinForm["id"]?>">
                        <?php foreach($signinForm['fields'] as $fieldName => $field): ?>
                            <div class="<?=implode(' ', $field["other"]["inputIconClass"])?>">
                                <input class="<?=$field["class"]?>" type="<?=$field["type"]?>" name="<?=$field["name"]?>" id="<?=$field["id"]?>" <?=array_keys($field, "required")[0]?>/>
                                <label class="placeholder" for="<?=$field["name"]?>"><?= empty($field['other']['label']) ? '' : Core\Core::translate('signin', $field['other']['label']); ?></label>
                            </div>
                        <?php endforeach; ?>
                    </form>
                    <button class="btn btn-rounded btn-primary w-100" type="submit" form="<?=$signinForm["id"]?>" value="Connexion"><?= Core\Core::translate('signin', 'signin') ?></button>
                </div>
                <div class="card-footer">
                    <p><?= Core\Core::translate('signin', 'dontHaveAccount') ?>
                        <a href="{% url user.show_signup_form %}"><?= Core\Core::translate('signin', 'register') ?></a>
                    </p>
                    <a class="click-to-open" href="#" data-modal="modal-forgotten-password"><?= Core\Core::translate('signin', 'forgotPassword') ?>?</a>
                </div>
                <div id="modal-forgotten-password" class="modal p-t-70">
                    <div class="modal-content">
                        <div class="modal-header">
                            <p class="text-subtitle"><?= Core\Core::translate('signin', 'forgotPassword') ?></p>
                            <span class="cancel close close-modal">&times;</span>
                        </div>
                        <div class="modal-body">
                            <p class="text-paragraph"><?= Core\Core::translate('signin', 'emailResetPassword') ?></p>
                            <form action="{% url <?= $forgottenPasswordForm["action"]?> %}" method="<?= $forgottenPasswordForm["method"]?>" id="<?= $forgottenPasswordForm["id"]?>">
                                <?php foreach($forgottenPasswordForm['fields'] as $fieldName => $field): ?>
                                    <div class="form-group">
                                        <label class="form-control-default-label" for="<?=$field["name"]?>"><?= empty($field['other']['label']) ? '' : Core\Core::translate('signin', $field['other']['label']); ?>:</label>
                                        <input class="<?=$field["class"]?>" type="<?=$field["type"]?>" name="<?=$field["name"]?>" id="<?=$field["id"]?>" <?=array_keys($field, "required")[0]?>>
                                    </div>
                                <?php endforeach; ?>
                            </form>   
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-success" type="submit" form="<?= $forgottenPasswordForm["id"]?>"><?= Core\Core::translate('signin', 'reset') ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
{% endblock content %}