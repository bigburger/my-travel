<?php

namespace App\Entity;

use ORM\Entity\BaseEntity;

class Locations extends BaseEntity
{
    protected $name;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set name.
     *
     * @param string $name
     * @return Locations
     */
    public function setName(string $name): Locations
    {
        $this->name = $name;

        return $this;
    }
}
