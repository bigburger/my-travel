<?php

namespace App\Entity;

use ORM\Entity\BaseEntity;

class Trips extends BaseEntity
{
    protected $user_id;
    protected $title;
    protected $description;
    protected $starting_date;
    protected $ending_date;
    protected $slug;
    protected $image_path;
    protected $creation_date;
    protected $update_date;

    public function __construct()
    {
        parent::__construct();
        $this->setIgnoredFields(['creation_date']);
    }

    /**
     * Get user id.
     *
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    /**
     * Set user id.
     *
     * @param int $user_id
     * @return Trips
     */
    public function setUserId(int $user_id): Trips
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Get title;
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Set title.
     *
     * @param string $title
     * @return Trips
     */
    public function setTitle(string $title): Trips
    {
        $this->title = htmlspecialchars($title);

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set description.
     *
     * @param string $description
     * @return Trips
     */
    public function setDescription(string $description): Trips
    {
        $this->description = htmlspecialchars($description);

        return $this;
    }

    /**
     * Get starting date.
     *
     * @return \DateTime
     * @throws \Exception
     */
    public function getStartingDate(): \DateTime
    {
        return new \DateTime($this->starting_date);
    }

    /**
     * Set starting date.
     *
     * @param $starting_date
     * @return Trips
     */
    public function setStartingDate($starting_date): Trips
    {
        if ($starting_date == null) {
            $this->starting_date = null;
        } elseif (gettype($starting_date) == 'string') {
            $this->starting_date = $starting_date;
        } else {
            $this->starting_date = $starting_date->getTimestamp();
        }
        return $this;
    }

    /**
     * Get ending date.
     *
     * @return \DateTime|null
     * @throws \Exception
     */
    public function getEndingDate()
    {
        return isset($this->ending_date) ? new \DateTime($this->ending_date) : null;
    }

    /**
     * Set ending date.
     *
     * @param $ending_date
     * @return Trips
     */
    public function setEndingDate($ending_date): Trips
    {
        if ($ending_date == null) {
            $this->ending_date = null;
        } elseif (gettype($ending_date) == 'string') {
            $this->ending_date = $ending_date;
        } else {
            $this->ending_date = $ending_date->getTimestamp();
        }

        return $this;
    }

    /**
     * Get slug.
     *
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * Set slug.
     *
     * @param string $slug
     * @return Trips
     */
    public function setSlug(string $slug): Trips
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get image_path.
     *
     * @return string|null
     */
    public function getImagePath(): ?string
    {
        return $this->image_path;
    }

    /**
     * Set image_path.
     *
     * @param string $image_path
     * @return Trips
     */
    public function setImagePath(string $image_path): Trips
    {
        $this->image_path = $image_path;

        return $this;
    }

    /**
     * Get description excerpt.
     *
     * @return string
     */
    public function getExcerpt(): ?string
    {
        return strlen($this->getDescription()) > 150 ?
            substr($this->getDescription(), 0, 150) . '...' : $this->getDescription();
    }

    /**
     * Get creation date.
     *
     * @return \DateTime
     * @throws \Exception
     */
    public function getCreationDate(): \DateTime
    {
        return new \DateTime($this->creation_date);
    }

    /**
     * Set creation date.
     *
     * @param \DateTime $creation_date
     * @return Trips
     */
    public function setCreationDate(\DateTime $creation_date): Trips
    {
        $this->creation_date = $creation_date->getTimestamp();
        return $this;
    }

    /**
     * Get update date.
     *
     * @return \DateTime
     * @throws \Exception
     */
    public function getUpdateDate(): \DateTime
    {
        return new \DateTime($this->update_date);
    }

    /**
     * Set update date.
     *
     * @param \DateTime $update_date
     * @return Trips
     */
    public function setUpdateDate(\DateTime $update_date): Trips
    {
        $this->update_date = $update_date->getTimestamp();

        return $this;
    }

    public function __toString()
    {
        return strval($this->id);
    }
}
