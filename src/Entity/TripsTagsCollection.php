<?php

namespace App\Entity;

use ORM\Entity\BaseEntityCollection;

class TripsTagsCollection extends BaseEntityCollection
{
    public function __construct(array $entities = [])
    {
        parent::__construct($entities);
    }
}
