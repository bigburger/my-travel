<?php

namespace App\Entity;

use ORM\Entity\BaseEntity;

class Comments extends BaseEntity
{
    protected $user_id;
    protected $trip_id;
    protected $text;
    protected $creation_date;
    protected $update_date;

    public function __construct()
    {
        parent::__construct();
        $this->setIgnoredFields(['creation_date']);
    }

    /**
     * Get writer id.
     *
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    /**
     * Set writer id.
     *
     * @param int $user_id
     *
     * @return Comments
     */
    public function setUserId(int $user_id): Comments
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Get trip id.
     *
     * @return int|null
     */
    public function getTripId(): ?int
    {
        return $this->trip_id;
    }

    /**
     * Set trip id.
     *
     * @param int $trip_id
     *
     * @return Comments
     */
    public function setTripId(int $trip_id): Comments
    {
        $this->trip_id = $trip_id;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * Set content.
     *
     * @param string $text
     */
    public function setText(string $text): Comments
    {
        $this->text = htmlspecialchars($text);

        return $this;
    }

    /**
     * Get creation date.
     *
     * @return \DateTime
     * @throws \Exception
     */
    public function getCreationDate(): \DateTime
    {
        return new \DateTime($this->creation_date);
    }

    /**
     * Set creation date.
     *
     * @param \DateTime $creation_date
     * @return Comments
     */
    public function setCreationDate(\DateTime $creation_date): Comments
    {
        $this->creation_date = $creation_date->getTimestamp();
        return $this;
    }

    /**
     * Get update date.
     *
     * @return \DateTime
     * @throws \Exception
     */
    public function getUpdateDate(): \DateTime
    {
        return new \DateTime($this->creation_date);
    }

    /**
     * Set update date.
     *
     * @param \DateTime $update_date
     * @return Comments
     */
    public function setUpdateDate(\DateTime $update_date): Comments
    {
        $this->update_date = $update_date->getTimestamp();

        return $this;
    }
}
