<?php

namespace App\Entity;

use ORM\Entity\BaseEntity;

class Contacts extends BaseEntity
{
    protected $user_id;
    protected $subject;
    protected $content;
    protected $creation_date;

    public function __construct()
    {
        parent::__construct();
        $this->setIgnoredFields(['creation_date']);
    }

    /**
     * Get user id.
     *
     * @return int
     */
    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    /**
     * Set user id.
     *
     * @param int $user_id
     * @return Contacts
     */
    public function setUserId(int $user_id): Contacts
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Get subject.
     *
     * @return string|null
     */
    public function getSubject(): ?string
    {
        return $this->subject;
    }

    /**
     * Set subject.
     *
     * @param string $subject
     *
     * @return Contacts
     */
    public function setSubject(string $subject): Contacts
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * Set content.
     *
     * @param $content
     * @return Contacts
     */
    public function setContent(string $content): Contacts
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get creation date.
     *
     * @return \DateTime
     * @throws \Exception
     */
    public function getCreationDate(): \DateTime
    {
        $date = new \DateTime();
        return $date->setTimestamp($this->creation_date ?? 0);
    }

    /**
     * Set creation date.
     *
     * @param \DateTime $creation_date
     * @return Contacts
     */
    public function setCreationDate(\DateTime $creation_date): Contacts
    {
        $this->creation_date = $creation_date->getTimestamp();
        return $this;
    }
}
