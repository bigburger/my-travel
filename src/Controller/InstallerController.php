<?php

namespace App\Controller;

use App\Entity\Users;
use Bodrum\Handler\DatabaseHandler;
use Core\Core;
use Form\FormHandler;
use Form\FormValidator;
use Router\Entity\BaseController;
use Router\Entity\Request;
use Router\Entity\ViewResponse;
use Router\Router;

class InstallerController extends BaseController
{

    /**
     * InstallerController constructor
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->installedMiddleware();
        $this->notAllowedIpMiddleware();
    }

    /**
     * Check if MyTravel was installed
     */
    private function installedMiddleware(): void
    {
        if (Core::getConfig('installer.installed')) {
            Router::redirect('home');
        }
    }

    /**
     *  Check the allowed address IP
     */
    private function notAllowedIpMiddleware(): void
    {
        if (Core::getConfig('installer.allowed_ip') != $this->request->getClientIp()
            && Router::getRouteByURI($this->request->getUri())->getName() != 'installer.ip_not_allowed') {
            Router::redirect('installer.ip_not_allowed');
        }
    }

    /**
     * Index of the MyTravel Installer
     */
    public function index(): void
    {
        $view  = new ViewResponse('installer.index');
        $indexForm = FormHandler::getForm('installer.index_setup');
        /* @var \Form\Entity\FormFieldType\SelectFormField $siteLanguageField */
        $siteLanguageField = $indexForm->getFields()['site_language'];

        foreach (Core::availableLanguages() as $language) {
            $siteLanguageField->addOption([
                "desc" => Core::translate('languages', $language),
                "value" => $language,
            ]);
        }

        $view->add('indexSetupForm', $indexForm->render());

        if ($_SESSION["ERROR_MESSAGE"]) {
            $view->add('errorMessage', $_SESSION["ERROR_MESSAGE"]);
            unset($_SESSION["ERROR_MESSAGE"]);
        }
    }

    /**
     * Check if index information's work
     */
    public function checkIndexSetup(): void
    {
        $formData = $this->request->getRequest();
        $formValidation = FormValidator::validateForm('installer.index_setup', $formData);

        if (!$formValidation["status"]) {
            $_SESSION["ERROR_MESSAGE"] = $formValidation["errorMessage"];
        } elseif (!Core::isAllowedLanguage($formData['site_language'])) {
            $_SESSION["ERROR_MESSAGE"] = "Langue non disponible";
        }

        if ($_SESSION["ERROR_MESSAGE"]) {
            Router::redirect('installer.index');
            return;
        }

        Core::setConfig('site.language', $formData['site_language']);

        Router::redirect('installer.db', 200);
    }

    /**
     * Ip not allowed to install MyTravel notifier
     */
    public function ipNotAllowed(): void
    {
        if (Core::getConfig('installer.allowed_ip') != $this->request->getClientIp()) {
            $view  = new ViewResponse('installer.ip_not_allowed');
            $view->add('ip', $this->request->getClientIp());
            return;
        }

        Router::redirect('installer.index');
    }

    /**
     *  Database setup
     */
    public function dbSetup(): void
    {
        $view  = new ViewResponse('installer.db_setup');
        $view->add('databaseConfig', Core::getConfig('database'));
        $view->add('dbSetupForm', FormHandler::getForm('installer.db_setup')->render());
        if ($_SESSION["ERROR_MESSAGE"]) {
            $view->add('errorMessage', $_SESSION["ERROR_MESSAGE"]);
            $view->add('form_params', $_SESSION["FORM_PARAMS"]);
            unset($_SESSION["FORM_PARAMS"]);
            unset($_SESSION["ERROR_MESSAGE"]);
        }
    }

    /**
     * Check if the database information's work
     */
    public function checkDbSetup(): void
    {
        $formData = $this->request->getRequest();
        $formValidation = FormValidator::validateForm('installer.db_setup', $formData);

        if (!$formValidation["status"]) {
            $_SESSION["ERROR_MESSAGE"] = $formValidation["errorMessage"];
            Router::redirect('installer.db');
            return;
        }

        try {
            $connection = new \PDO(
                $formData['driver'] .
                ':host=' . $formData['host'] .
                ';port=' . $formData['port'] .
                ';dbname=' . $formData['name'],
                $formData['user'],
                $formData['password']
            );
        } catch (\Exception $e) {
            $_SESSION["ERROR_MESSAGE"] = "La connexion à la base de données a échoué";
            $_SESSION["FORM_PARAMS"] = $formData;
            Router::redirect('installer.db');
            return;
        }

        Core::setConfig('database.driver', $formData['driver']);
        Core::setConfig('database.host', $formData['host']);
        Core::setConfig('database.port', $formData['port']);
        Core::setConfig('database.name', $formData['name']);
        Core::setConfig('database.user', $formData['user']);
        Core::setConfig('database.password', $formData['password']);

        $dbHandler = new DatabaseHandler();
        if (!$dbHandler->rebuild()) {
            $_SESSION["ERROR_MESSAGE"] = "Database generation failed";
            Router::redirect('installer.db');
            return;
        }

        Router::redirect('installer.admin', 200);
    }

    /**
     * Administrator account setup
     */
    public function adminSetup(): void
    {
        $view  = new ViewResponse('installer.admin_setup');
        $view->add('adminSetupForm', FormHandler::getForm('installer.admin_setup')->render());
        if ($_SESSION["ERROR_MESSAGE"]) {
            $view->add('errorMessage', $_SESSION["ERROR_MESSAGE"]);
            $view->add('form_params', $_SESSION["FORM_PARAMS"]);
            unset($_SESSION["FORM_PARAMS"]);
            unset($_SESSION["ERROR_MESSAGE"]);
        }
    }

    /**
     * Check if the account information's are valid to create the administrator account
     */
    public function checkAdminSetup(): void
    {
        $formData = $this->request->getRequest();
        $formValidation = FormValidator::validateForm('installer.admin_setup', $formData);

        if (!$formValidation["status"]) {
            $_SESSION["ERROR_MESSAGE"] = $formValidation["errorMessage"];
        } elseif ($formData["password"] != $formData["confirm_password"]) {
            $_SESSION["ERROR_MESSAGE"] = "Mots de passes différents";
        }

        if ($_SESSION["ERROR_MESSAGE"]) {
            $_SESSION["FORM_PARAMS"] = $formData;
            Router::redirect('installer.admin');
            return;
        }

        $userToSignup = new Users();
        $userToSignup->setEmail($formData["email"]);
        $userToSignup->setRawPassword($formData["password"]);
        $userToSignup->setFirstname($formData["firstname"]);
        $userToSignup->setLastname($formData["lastname"]);
        $userToSignup->setUsername($formData["username"]);
        $userToSignup->setRole('admin');
        $userToSignup->setStatus(1);

        // Saving new user to db
        $userToSignup->save();
        // Login new user directly after
        $userToSignup->login();
        // Redirect user to trips page
        Router::redirect('installer.plugins', 200);
    }

    /**
     * Plugins setup
     */
    public function pluginsSetup(): void
    {
        $view  = new ViewResponse('installer.plugins_setup');
        $view->add('pluginsSetupForm', FormHandler::getForm('installer.plugins_setup')->render());
        if ($_SESSION["ERROR_MESSAGE"]) {
            $view->add('errorMessage', $_SESSION["ERROR_MESSAGE"]);
            $view->add('form_params', $_SESSION["FORM_PARAMS"]);
            unset($_SESSION["FORM_PARAMS"]);
            unset($_SESSION["ERROR_MESSAGE"]);
        }
    }

    /**
     * Check if the plugins information's are valid
     */
    public function checkPluginsSetup(): void
    {
        $formData = $this->request->getRequest();
        $formValidation = FormValidator::validateForm('installer.plugins_setup', $formData);

        if (!$formValidation["status"]) {
            $_SESSION["ERROR_MESSAGE"] = $formValidation["errorMessage"];
            $_SESSION["FORM_PARAMS"] = $formData;
            Router::redirect('installer.plugins');
            return;
        }

        Core::setConfig('recaptcha.site_key', $formData['recaptchaSiteKey']);
        Core::setConfig('recaptcha.private_key', $formData['recaptchaPrivateKey']);
        Core::setConfig('tinymce.key', $formData['tinymceKey']);

        Router::redirect('installer.finish', 200);
    }

    /**
     * Finish the setup
     */
    public function finishSetup(): void
    {
        $view  = new ViewResponse('installer.finish_setup');
        $view->add('finishSetupForm', FormHandler::getForm('installer.finish_setup')->render());
        $view->add('availableLanguages', Core::availableLanguages());
        if ($_SESSION["ERROR_MESSAGE"]) {
            $view->add('errorMessage', $_SESSION["ERROR_MESSAGE"]);
            $view->add('form_params', $_SESSION["FORM_PARAMS"]);
            unset($_SESSION["FORM_PARAMS"]);
            unset($_SESSION["ERROR_MESSAGE"]);
        }
    }

    /**
     * Check if the CMS information's are valid to finish the setup
     */
    public function checkFinishSetup(): void
    {
        $formData = $this->request->getRequest();
        $formValidation = FormValidator::validateForm('installer.finish_setup', $formData);

        if (!$formValidation["status"]) {
            $_SESSION["ERROR_MESSAGE"] = $formValidation["errorMessage"];
        }

        if ($_SESSION["ERROR_MESSAGE"]) {
            $_SESSION["FORM_PARAMS"] = $formData;
            Router::redirect('installer.finish');
            return;
        }

        Core::setConfig('site.name', htmlspecialchars($formData['site_name']));
        Core::setConfig('site.description', htmlspecialchars($formData['site_desc']));
        Core::setConfig('signup.sendMail', $formData['signup_sendmail'] ? true : false);

        Core::setConfig('site.footer.social_networks.facebook', htmlspecialchars($formData['facebook_url']));
        Core::setConfig('site.footer.social_networks.twitter', htmlspecialchars($formData['twitter_url']));
        Core::setConfig('site.footer.social_networks.instagram', htmlspecialchars($formData['instagram_url']));

        Core::setConfig('site.footer.contact.mail', htmlspecialchars($formData['contact_mail']));
        Core::setConfig('site.footer.contact.phone', htmlspecialchars($formData['contact_phone']));

        Core::setConfig('installer.installed', true);

        Router::redirect('home', 200);
    }
}
