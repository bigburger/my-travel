<?php

namespace App\Controller;

use Router\Entity\BaseController;
use Router\Router;
use App\Repository\CommentsRepository;

class CommentController extends BaseController
{
    /**
     * Delete a comment
     */
    public function deleteComment()
    {
        $tripCommentData = $this->request->getRequest();

        $commentsRepo = new CommentsRepository();
        $commentsRepo->deleteComment([
            ["id", "=", intval($tripCommentData['comment-id'])]
        ]);

        $_SESSION["SUCCESS_MESSAGE"] = "commentDeletedSuccess";
        Router::redirect('/trip?trip_id=' . $tripCommentData['trip-id']);
    }

    /**
     * Update a comment
     */
    public function updateComment()
    {
        $tripCommentData = $this->request->getRequest();
        $commentsRepo = new CommentsRepository();

        $commentsRepo->updateComment([
            'id' => $tripCommentData['comment-id'],
            'text' => $tripCommentData['updated-comment-' . $tripCommentData['comment-id']]
        ]);

        $_SESSION["SUCCESS_MESSAGE"] = "commentEditedSuccess";
        Router::redirect('/trip?trip_id=' . $tripCommentData['trip-id']);
    }
}
