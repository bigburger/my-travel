<?php

namespace App\Controller;

use Bodrum\Handler\DatabaseHandler;
use Core\Core;
use Form\FormHandler;
use Form\FormValidator;
use Router\Entity\BaseController;
use Router\Entity\ViewResponse;
use Router\Router;
use App\Repository\UsersRepository;
use App\Repository\TripsRepository;
use App\Repository\StepsRepository;
use App\Repository\CommentsRepository;
use App\Repository\PagesRepository;

class AdminController extends BaseController
{
    /**
     * index action
     */
    public function index(): void
    {
        $usersRepo = new UsersRepository();
        $tripsRepo = new TripsRepository();
        $stepsRepo = new StepsRepository();
        $commentsRepo = new CommentsRepository();

        $users = $usersRepo->getAllUsers();
        $trips = $tripsRepo->getAllTrips();
        $steps = $stepsRepo->getAllSteps();
        $comments = $commentsRepo->getAllComments();
        $entities = ['users', 'trips', 'steps', 'comments'];

        $view = new ViewResponse('admin_dashboard');

        foreach ($entities as $entity) {
            ${$entity . 'ByDate'} = [];
            $week = [];

            for ($i = 0; $i < 7; $i++) {
                $dayDate = new \DateTime();
                if ($i > 0) {
                    $dayDate->modify('-' . $i . ' days');
                }
                $dayDate->setTime(0, 0, 0, 0);
                $week[$dayDate->getTimestamp()] = $dayDate->getTimestamp();
                ${$entity . 'ByDate'}[$dayDate->getTimestamp()] = 0;
            }

            foreach ($$entity as $entityRow) {
                $creationDate = $entityRow->getCreationDate()->setTime(0, 0, 0, 0)->getTimestamp();
                ${$entity . 'ByDate'}[$creationDate] += 1;
            }

            // usersByDate, tripsByDate, stepsByDate, commentsByDate are available in the view
            $view->add($entity . 'ByDate', array_reverse(array_intersect_key(${$entity . 'ByDate'}, $week)));
        }

        $view->add('usersCount', count($users));
        $view->add('tripsCount', count($trips));
        $view->add('stepsCount', count($steps));
        $view->add('commentsCount', count($comments));
    }

    /**
     * usersList page action
     */
    public function usersList(): void
    {
        $view = new ViewResponse('admin_users');
        $usersRepo = new UsersRepository();

        $searchQuery = $this->request->getRequest()["searchQuery"];
        $users = $searchQuery ? $usersRepo->getAllUsersLike($searchQuery)
            : $usersRepo->getAllUsers('creation_date desc');

        if ($_SESSION["SUCCESS_MESSAGE"]) {
            $view->add('successMessage', $_SESSION["SUCCESS_MESSAGE"]);
            $_SESSION["SUCCESS_MESSAGE"] = null;
        }

        $view->add('users', $users);
        $view->add('searchQuery', $searchQuery);
    }

    /**
     * commentsList page action
     */
    public function commentsList(): void
    {
        $view = new ViewResponse('admin_comments');
        $commentsRepo = new CommentsRepository();

        $searchQuery = $this->request->getRequest()["searchQuery"];
        $comments = $searchQuery ? $commentsRepo->getAllCommentsLike($searchQuery)
            : $commentsRepo->getAllComments('creation_date desc');

        if ($_SESSION["SUCCESS_MESSAGE"]) {
            $view->add('successMessage', $_SESSION["SUCCESS_MESSAGE"]);
            $_SESSION["SUCCESS_MESSAGE"] = null;
        }

        $view->add('comments', $comments);
        $view->add('usersRepo', new UsersRepository());
        $view->add('searchQuery', $searchQuery);
    }

    /*
     * Delete a comment
     */
    public function deleteComment(): void
    {
        $commentId = $this->request->getRequest()['comment-id'];

        if ($commentId) {
            $commentsRepo = new CommentsRepository();
            $commentsRepo->deleteComment([
                ["id", "=", $commentId]
            ]);
            $_SESSION["SUCCESS_MESSAGE"] = "commentDeletedSuccess";
        }
        Router::redirect('back.comments');
    }

    /**
     * tripsList page action
     */
    public function tripsList(): void
    {
        $view = new ViewResponse('admin_trips');
        $tripsRepo = new TripsRepository();

        $searchQuery = $this->request->getRequest()["searchQuery"];
        $trips = $searchQuery ? $tripsRepo->getAllTripsLike($searchQuery)
            : $tripsRepo->getAllTrips('creation_date desc');

        if ($_SESSION["SUCCESS_MESSAGE"]) {
            $view->add('successMessage', $_SESSION["SUCCESS_MESSAGE"]);
            $_SESSION["SUCCESS_MESSAGE"] = null;
        }

        $view->add('trips', $trips);
        $view->add('usersRepo', new UsersRepository());
        $view->add('searchQuery', $searchQuery);
    }

    /*
     * Delete a trip
     */
    public function deleteTrip(): void
    {
        $tripId = $this->request->getRequest()['trip-id'];

        if ($tripId) {
            $tripsRepo = new TripsRepository();
            $tripsRepo->deleteTrip([
                ["id", "=", $tripId]
            ]);
            $_SESSION["SUCCESS_MESSAGE"] = "tripDeletedSuccess";
        }
        Router::redirect('back.trips');
    }

    /**
     * stepsList page action
     */
    public function stepsList(): void
    {
        $view = new ViewResponse('admin_steps');
        $stepsRepo = new StepsRepository();

        $searchQuery = $this->request->getRequest()["searchQuery"];
        $steps = $searchQuery ? $stepsRepo->getAllStepsLike($searchQuery)
            : $stepsRepo->getAllSteps('creation_date desc');

        if ($_SESSION["SUCCESS_MESSAGE"]) {
            $view->add('successMessage', $_SESSION["SUCCESS_MESSAGE"]);
            $_SESSION["SUCCESS_MESSAGE"] = null;
        }

        $view->add('steps', $steps);
        $view->add('tripsRepo', new TripsRepository());
        $view->add('searchQuery', $searchQuery);
    }

    /*
     * Delete a step
     */
    public function deleteStep(): void
    {
        $stepId = $this->request->getRequest()['step-id'];

        if ($stepId) {
            $stepsRepo = new StepsRepository();
            $stepsRepo->deleteStep([
                ["id", "=", $stepId]
            ]);
            $_SESSION["SUCCESS_MESSAGE"] = Core::translate('admin.step', 'deletedStepConfirm');
        }
        Router::redirect('back.steps');
    }
 
    /**
     * Site settings action
     */
    public function siteSettings(): void
    {
        $view = new ViewResponse('admin_settings');

        $generalSettingsForm = FormHandler::getForm('back.site_settings_general');
        /* @var \Form\Entity\FormFieldType\SelectFormField $siteLanguageField */
        $siteLanguageField = $generalSettingsForm->getFields()['site_language'];

        foreach (Core::availableLanguages() as $language) {
            $siteLanguageField->addOption([
                "desc" => Core::translate('languages', $language),
                "value" => $language,
            ]);
        }

        $view->add('generalSettingsForm', $generalSettingsForm->render());
        $view->add(
            'socialNetworksSettingsForm',
            FormHandler::getForm('back.site_settings_social_networks')->render()
        );
        $view->add('contactSettingsForm', FormHandler::getForm('back.site_settings_contact')->render());
        $view->add('graphicsSettingsForm', FormHandler::getForm('back.site_settings_graphics')->render());
        $view->add('pluginsSettingsForm', FormHandler::getForm('back.site_settings_plugins')->render());
        $view->add('databaseSettingsForm', FormHandler::getForm('back.site_settings_database')->render());

        $view->add('facebook_url', Core::getConfig('site.footer.social_networks.facebook'));
        $view->add('twitter_url', Core::getConfig('site.footer.social_networks.twitter'));
        $view->add('instagram_url', Core::getConfig('site.footer.social_networks.instagram'));

        $view->add('contact_email', Core::getConfig('site.footer.contact.mail'));
        $view->add('contact_phone', Core::getConfig('site.footer.contact.phone'));

        $view->add('recaptcha_private_key', Core::getConfig('recaptcha.private_key'));
        $view->add('database_driver', Core::getConfig('database.driver'));
        $view->add('database_host', Core::getConfig('database.host'));
        $view->add('database_port', Core::getConfig('database.port'));
        $view->add('database_name', Core::getConfig('database.name'));
        $view->add('database_user', Core::getConfig('database.user'));
        $view->add('database_password', Core::getConfig('database.password'));

        if ($_SESSION["ERROR_MESSAGE"]) {
            $view->add('errorMessage', $_SESSION["ERROR_MESSAGE"]);
            unset($_SESSION["ERROR_MESSAGE"]);
        } elseif ($_SESSION["SUCCESS_MESSAGE"]) {
            $view->add('successMessage', $_SESSION["SUCCESS_MESSAGE"]);
            unset($_SESSION["SUCCESS_MESSAGE"]);
        }
    }

    /**
     * Update general site settings action
     */
    public function updateGeneralSiteSettings(): void
    {
        $formData = $this->request->getRequest();
        $formValidation = FormValidator::validateForm('back.site_settings_general', $formData);

        if (!$formValidation["status"]) {
            $_SESSION["ERROR_MESSAGE"] = $formValidation["errorMessage"];
        } elseif (!Core::isAllowedLanguage($formData['site_language'])) {
            $_SESSION["ERROR_MESSAGE"] = Core::translate('admin.setting', 'notAvailableLanguage');
        }

        if ($_SESSION["ERROR_MESSAGE"]) {
            Router::redirect('back.site.settings');
            return;
        }

        Core::setConfig('site.name', htmlspecialchars($formData['site_name']));
        Core::setConfig('site.description', htmlspecialchars($formData['site_desc']));
        Core::setConfig('site.language', $formData['site_language']);
        Core::setConfig('signup.sendMail', $formData['signup_sendmail'] ? true : false);

        $_SESSION["SUCCESS_MESSAGE"] = Core::translate('admin.setting', 'generalSettingsUpdateConfim');

        Router::redirect('back.site.settings', 200);
    }

    /**
     * Update social networks settings action
     */
    public function updateSocialNetworksSiteSettings(): void
    {
        $formData = $this->request->getRequest();
        $formValidation = FormValidator::validateForm('back.site_settings_social_networks', $formData);

        if (!$formValidation["status"]) {
            $_SESSION["ERROR_MESSAGE"] = $formValidation["errorMessage"];
        }

        if ($_SESSION["ERROR_MESSAGE"]) {
            Router::redirect('back.site.settings');
            return;
        }

        Core::setConfig('site.footer.social_networks.facebook', htmlspecialchars($formData['facebook_url']));
        Core::setConfig('site.footer.social_networks.twitter', htmlspecialchars($formData['twitter_url']));
        Core::setConfig('site.footer.social_networks.instagram', htmlspecialchars($formData['instagram_url']));


        $_SESSION["SUCCESS_MESSAGE"] = Core::translate('admin.setting', 'socialNetworksSettingsUpdateConfim');

        Router::redirect('back.site.settings', 200);
    }

    /**
     * Update social networks settings action
     */
    public function updateContactSiteSettings(): void
    {
        $formData = $this->request->getRequest();
        $formValidation = FormValidator::validateForm('back.site_settings_contact', $formData);

        if (!$formValidation["status"]) {
            $_SESSION["ERROR_MESSAGE"] = $formValidation["errorMessage"];
        }

        if ($_SESSION["ERROR_MESSAGE"]) {
            Router::redirect('back.site.settings');
            return;
        }

        Core::setConfig('site.footer.contact.mail', htmlspecialchars($formData['contact_mail']));
        Core::setConfig('site.footer.contact.phone', htmlspecialchars($formData['contact_phone']));


        $_SESSION["SUCCESS_MESSAGE"] = Core::translate('admin.setting', 'contactSettingsUpdateConfim');

        Router::redirect('back.site.settings', 200);
    }

    /**
     * Update graphics site settings action
     */
    public function updateGraphicsSiteSettings(): void
    {
        $formData = $this->request->getRequest();
        $formValidation = FormValidator::validateForm('back.site_settings_graphics', $formData);

        if (!$formValidation["status"]) {
            $_SESSION["ERROR_MESSAGE"] = $formValidation["errorMessage"];
        }

        if ($_SESSION["ERROR_MESSAGE"]) {
            Router::redirect('back.site.settings');
            return;
        }

        $logo1 = $this->request->getFiles()['logo1'];
        if ($logo1['name'] != "") {
            FormHandler::uploadFile(
                $logo1,
                'logo-primary.svg',
                'images/logos/'
            );
        }

        $logo2 = $this->request->getFiles()['logo2'];
        if ($logo2['name'] != "") {
            FormHandler::uploadFile(
                $logo2,
                'logo-secondary.svg',
                'images/logos/'
            );
        }

        $logo3 = $this->request->getFiles()['logo3'];
        if ($logo3['name'] != "") {
            FormHandler::uploadFile(
                $logo3,
                'logo-tertiary.svg',
                'images/logos/'
            );
        }

        $logo4 = $this->request->getFiles()['logo4'];
        if ($logo4['name'] != "") {
            FormHandler::uploadFile(
                $logo4,
                'logo-quaternary.svg',
                'images/logos/'
            );
        }

        $favicon = $this->request->getFiles()['favicon'];
        if ($favicon['name'] != "") {
            FormHandler::uploadFile(
                $favicon,
                'logo-icon.ico',
                'images/icons/'
            );
        }

        Core::setConfig('theme.name', $formData['theme_name']);

        $_SESSION["SUCCESS_MESSAGE"] = Core::translate('admin.setting', 'graphicSettingsUpdateConfim');

        Router::redirect('back.site.settings', 200);
    }

    /**
     * Update plugins site settings action
     */
    public function updatePluginsSiteSettings(): void
    {
        $formData = $this->request->getRequest();
        $formValidation = FormValidator::validateForm('back.site_settings_plugins', $formData);

        if (!$formValidation["status"]) {
            $_SESSION["ERROR_MESSAGE"] = $formValidation["errorMessage"];
        }

        if ($_SESSION["ERROR_MESSAGE"]) {
            Router::redirect('back.site.settings');
            return;
        }

        Core::setConfig('recaptcha.site_key', $formData['recaptcha_site_key']);
        Core::setConfig('recaptcha.private_key', $formData['recaptcha_private_key']);
        Core::setConfig('tinymce.key', $formData['tinymce_key']);

        $_SESSION["SUCCESS_MESSAGE"] = Core::translate('admin.setting', 'pluginsSettingsUpdateConfim');

        Router::redirect('back.site.settings', 200);
    }

    /**
     * Update database site settings action
     */
    public function updateDatabaseSiteSettings(): void
    {
        $formData = $this->request->getRequest();
        $formValidation = FormValidator::validateForm('back.site_settings_database', $formData);

        if (!$formValidation["status"]) {
            $_SESSION["ERROR_MESSAGE"] = $formValidation["errorMessage"];
        }

        if ($_SESSION["ERROR_MESSAGE"]) {
            Router::redirect('back.site.settings');
            return;
        }

        try {
            $connection = new \PDO(
                $formData['driver'] .
                ':host=' . $formData['host'] .
                ';port=' . $formData['port'] .
                ';dbname=' . $formData['name'],
                $formData['user'],
                $formData['password']
            );
        } catch (\Exception $e) {
            $_SESSION["ERROR_MESSAGE"] = Core::translate('admin.setting', 'dbConnexionFail');
            Router::redirect('back.site.settings');
            return;
        }

        Core::setConfig('database.driver', $formData['driver']);
        Core::setConfig('database.host', $formData['host']);
        Core::setConfig('database.port', $formData['port']);
        Core::setConfig('database.name', $formData['name']);
        Core::setConfig('database.user', $formData['user']);
        Core::setConfig('database.password', $formData['password']);

        $dbHandler = new DatabaseHandler();
        if (!$dbHandler->generate()) {
            $_SESSION["ERROR_MESSAGE"] = "Database generation failed";
            Router::redirect('back.site.settings');
            return;
        }

        $_SESSION["SUCCESS_MESSAGE"] = Core::translate('admin.setting', 'dbSettingsUpdateConfim');

        Router::redirect('back.site.settings', 200);
    }

    /**
     * pagesList page action
     */
    public function pagesList(): void
    {
        $pagesRepo = new PagesRepository();
        $view = new ViewResponse('admin_pages');

        $searchQuery = $this->request->getRequest()["searchQuery"];
        $pages = $searchQuery ? $pagesRepo->getAllPagesLike($searchQuery) :
            $pagesRepo->getAllPages('creation_date desc');

        if ($_SESSION["ERROR_MESSAGE"]) {
            $view->add('errorMessage', $_SESSION["ERROR_MESSAGE"]);
            $_SESSION["ERROR_MESSAGE"] = null;
        } elseif ($_SESSION["SUCCESS_MESSAGE"]) {
            $view->add('successMessage', $_SESSION["SUCCESS_MESSAGE"]);
            $_SESSION["SUCCESS_MESSAGE"] = null;
        }

        $view->add('pages', $pages);
        $view->add('searchQuery', $searchQuery);
    }

    /*
     * Delete a page
     */
    public function deletePage(): void
    {
        $pageId = $this->request->getRequest()['page-id'];

        if ($pageId) {
            $pagesRepo = new PagesRepository();
            $pagesRepo->deletePage([
                ["id", "=", $pageId]
            ]);
            $_SESSION["SUCCESS_MESSAGE"] = Core::translate('admin.page', 'deletedPageConfirm');
        }
        Router::redirect('back.pages');
    }

    /**
     * editPage action
     */
    public function editPage(): void
    {
        $view = new ViewResponse('admin_edit_page');
    }
    
    /*
     * Delete a user
     */
    public function deleteUser(): void
    {
        $user = $this->request->getRequest()['user-id'];
        if ($user != Core::getCurrentUser()->getId()) {
            $usersRepo = new UsersRepository();
            $usersRepo->deleteUser([
                ["id", "=", intval($user)]
            ]);
            $_SESSION["SUCCESS_MESSAGE"] = Core::translate('admin.page', 'deletedPageConfirm');
        }
        Router::redirect('back.users');
    }

    /*
     * Update a user
     */
    public function updateUser(): void
    {
        $usersRepo = new UsersRepository();
        $usersRepo->updateUser($this->request->getRequest());
        $_SESSION["SUCCESS_MESSAGE"] = Core::translate('admin.users', 'updatedUserConfirm');
        Router::redirect('back.users');
    }

    public function addPage(): void
    {
        $pagesRepo = new PagesRepository();
        $requestData = $this->request->getRequest();

        if (count($pagesRepo->getLocationPages($requestData["page-location"])) >= 5) {
            $_SESSION["ERROR_MESSAGE"] = Core::translate('admin.page', 'limitLocation');
        } elseif ($pagesRepo->getOnePageBy([["slug", "=", $requestData["page-slug"]]])) {
            $_SESSION["ERROR_MESSAGE"] = Core::translate('admin.page', 'identifiantAlreadyExist');
        } elseif (empty(trim($requestData['page-content']))) {
            $_SESSION["ERROR_MESSAGE"] = Core::translate('admin.page', 'identifiantAlreadyExist');
        } elseif (!preg_match("/^[a-z0-9_-]+$/i", $requestData["page-slug"])) {
            $_SESSION["ERROR_MESSAGE"] = Core::translate('admin.page', 'specialChars');
        } elseif (strlen($requestData["page-name"]) > 30) {
            $_SESSION["ERROR_MESSAGE"] = Core::translate('admin.page', 'tooBigName');
        }

        if ($_SESSION["ERROR_MESSAGE"]) {
            Router::redirect('back.pages');
            return;
        }

        $pagesRepo->createPage($requestData);
        $_SESSION["SUCCESS_MESSAGE"] = Core::translate('admin.page', 'createdPageConfirm');
        Router::redirect('back.pages');
    }

    /*
     * Update a page
     */
    public function updatePage(): void
    {
        $pagesRepo = new PagesRepository();
        $requestData = $this->request->getRequest();

        if ($requestData["old-page-location"] != $requestData["page-location"]
            && count($pagesRepo->getLocationPages($requestData["page-location"])) >= 5) {
            $_SESSION["ERROR_MESSAGE"] = Core::translate('admin.page', 'limitLocation');
        } elseif ($requestData["old-page-slug"] != $requestData["page-slug"]
            && $pagesRepo->getOnePageBy([["slug", "=", $requestData["page-slug"]]])) {
            $_SESSION["ERROR_MESSAGE"] = Core::translate('admin.page', 'identifiantAlreadyExist');
        } elseif (empty(trim($requestData['page-content']))) {
            $_SESSION["ERROR_MESSAGE"] = Core::translate('admin.page', 'contentCantEmpty');
        } elseif (!preg_match("/^[a-z0-9_-]+$/i", $requestData["page-slug"])) {
            $_SESSION["ERROR_MESSAGE"] = Core::translate('admin.page', 'specialChars');
        } elseif (strlen($requestData["page-name"]) > 30) {
            $_SESSION["ERROR_MESSAGE"] = Core::translate('admin.page', 'tooBigName');
        }

        if ($_SESSION["ERROR_MESSAGE"]) {
            Router::redirect('back.pages');
            return;
        }

        $pagesRepo->updatePage($requestData);
        $_SESSION["SUCCESS_MESSAGE"] = Core::translate('admin.page', 'updatedPageConfirm');
        Router::redirect('back.pages');
    }
}
