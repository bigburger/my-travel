<?php

namespace App\Controller;

use Router\Entity\BaseController;
use Router\Entity\ViewResponse;
use App\Repository\TripsRepository;

class HomeController extends BaseController
{
    /**
     * Index of the website
     */
    public function index(): void
    {
        $tripsRepo = new TripsRepository();
        $view  = new ViewResponse('home');
        $view->add('lastTrips', $tripsRepo->LastHomeTrips());
    }
}
