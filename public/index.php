<?php

session_start();

$GLOBALS['BASE_DIR'] = substr(__DIR__, 0, -6);

require_once $GLOBALS['BASE_DIR'] . 'vendor/Autoloader.php';

spl_autoload_register('loadClass');

function loadClass(string $class): void
{
    App\Autoloader::loader($class);
}

use Logger\Logger;
use Router\Router;
use Router\Exception\ServerErrorException;
use Core\Core;

// Launch the installer if the env.yml doesn't exist or MyTravel wasn't installed.
if (!file_exists(Core::getConfigFilePath())) {
    copy($GLOBALS['BASE_DIR'] . "env.yml.dist", $GLOBALS['BASE_DIR'] . "env.yml");
}

// Declare globals var's
$GLOBALS['ENVIRONMENT'] = Core::getConfig('environment') === "production" ? "production" : "development";

// Routing system
$request = Router::getRequest();
$route = Router::getRouteByRequest($request);

if (Core::getConfig('installer.installed') != true) {
    if ($route->getClass() != 'InstallerController') {
        $route = Router::getRouteByNameForRender('installer.index');
    }
} else {
    // Authorize or not the current user to access the requested route
    if (!Router::isCurrentUserAuthorized($route)) {
        Router::redirectUnauthorizedUser($route);
    }
}

$class = "\\App\\Controller\\". $route->getClass();
try {
    if (class_exists($class)) {
        $classObject = new $class($request);
        $action = $route->getAction();
        if (method_exists($classObject, $action)) {
            $classObject->$action();
        } else {
            throw new ServerErrorException("Error on action $action check exist.");
        }
    } else {
        throw new ServerErrorException("Error on class " . $route->getClass() . " check exist.");
    }
} catch (ServerErrorException $e) {
    $logger = new Logger();
    $logger->error($e->getMessage());
}
